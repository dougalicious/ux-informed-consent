# UX Informed Consent V3 #

prod: ["LIVE 10/15/2020 - UX Informed Consent Tool Logic (go/uxiclogic) [v3]"](https://docs.google.com/spreadsheets/d/1ROL_Xp3r7JRhb4zjQF9pGyv9uKA0CmeuPCr10zMX0hQ/edit#gid=2002425214)

dev: [Staging 01/27/2021 - UX Informed Consent Tool Logic (go/uxiclogic)](https://docs.google.com/spreadsheets/d/1MoOeqnmNxOKjMSIDS_buteMtKwCvxR3CmG1vM6DsDOA/edit?resourcekey=0-0jqI7khLwTgEUaVKHJNXaQ#gid=2002425214)

## Objective {#objective}

*Automate UX Informed Consent Notification where the content of the email is build based on the the form selections of Q&A, Google Apps Script, and Spreadsheet. The Trix is used as a CMS to manage various logic flows, and hyperlinks.*

## Background {#background}

* v1 - ???
* v2 - before 10/15/2020
* v3 - current
*various improvement/bug/feature updates have been made. Please note the existing code was build in combination of v1/v2 design and practice.  Various areas where legacy codes exist and may still being used, sometime in 2020 the Google Apps Script env shifted to V8 engine however legacy codes abids by ES5 || ECMA2009*
***not*** *write about the design or ideas to solve problems stated here.*


## Overview {#overview}

*The UXIC works to take form submission answers and generate a custome html email based on critera selected*

## Infrastructure {#infrastructure}

*Triggers:*
  * function passSheetDataToOnFormSubmit is setup to run at some minute interval where the code looks for any new submission, and then works to process them for an outbound email. this code is setup as no event trigger exist for the app script to recognized a new form submission. Therefore the script also generates an artificial event similar to a onFormSubmit. The data get added to the trix via a CMS template and external form URL build by Kyle, where the script works to append the data on to the trix.
  * other triggers: there's other triggers that run, however this was based on legacy code, and hasn't been revised. Therefore an assumption of it not broken, don't fix.

*Service include: *Gmail, Admin Directory* 

## Detailed Design {#design}

*The code is setup to process each row as a single submission, relative to the column name "Email Sent" being blank or not. if blank it works to send the email and populate cell with datetime.*
* General Schema
  * Evaluate form answers to determine Data type, participant type, countries. this data is then used to derive html content relative to various sheet names: "Data Types Permutations" , "Key", "Storing Powerform Links", "Region Map", "PCounsel next steps matrix", "Who to cc", "UXR next steps matrix", "Headers matrix", "Next steps key", "Notes Matrix"

* Detail Code flow
  * Get Data parameters
    * keyDataBySheet - determines form (question/answers) to "script variable name" (legacy schema)
    * determines countrys, as well as *sortedCountries* work done by Kyle to account for 'Google FTE' powerform links that meets criteria
    * determines Enterprise participant type
    * determines Powerform links which match critera of dataTypes, countries, participant types
    * get next steps values relating to "UXR next steps matrix", "Headers matrix", "Next steps key", "Notes Matrix"
  * Generate Email Template
    * Based on form answer selection works to generate html output to includes the various parameters mentioned before.
    * General composition schema works to match text phrases such as *[Body UXR-facing]* with there corresponding pieces of information.
    * marjority of work is done as fn: addTextToBody
      * Display Tables, and find and replace html chunks
  * Send Email
    * populate's cc'ing emails, {from, replyTo, cc, htmlBody }parameter
    * also populates "CC'd" Column in trix
  * Update Trix column field "Email Sent" under "Form Responses 2" sheet


* Quick summary
* Version
* [bitbucket link](https://bitbucket.org/dougalicious/ux-informed-consent/src/master/)

### How do I get set up? ###

* Summary of set up
  * clone existing repository
  * go into ./prod & ./dev and do a clasp pull to get current version

### Who do I talk to? ###

* Repo owner: douglascox@google.com 
* contacts: uxi-automation@google.com 


### Sheets Info Details ###

* "Form Responses 2" -contains all the "appendRow" from CMS Google Apps Script Form
* "Key"  - Contains the association of "Form Text" associated to "Script Variable Name" which is used through Matrix Sheets, and internal code flow. This also reference "Email" words and "Data Type" column which is used to determine "Key" type from "Data Types Permulations"
* "Data Types Permutations" column video, bio, spii, pii are used to determin the "Key" which has an associate "Data" on "Storing Powerform Links" sheet
* "Region Map" - when selecting a country this sheet is used to determine its region, and whether or not "eSignatures" are accepted. the "Country" column is match via entry from the form where an exact match is used to derive the region, and eSignature properties. Also if a country is typed in manually and exact matches a country in the "Country" column it will be picked up with properties. Default properties for non match countries related to "Other" on the "Storing Powerform Links" on "Country+Language"
* "PCounsel next steps matrix" - column A list available data type options in relation to sheet name "Key" which maps to the answer submission. For each column where Region, data type, and participant type matches an HTML Chunk is selected. This are listed in order of priorites which can be found on "Next steps key" **Please note** for Pcounsel i believe only the highiest ranking Html Chunk is used. The "Next steps key" sheet also determines subject header to include either "fyi" or "actionrequired"
* "Who to cc" maps combination of "Participant Type" with "Data Type" to add relativant Pcounsel alias. **Please note** these "Data Type" are relative to the "Script Variable Name" on the "Key" sheet
* "UXR next steps matrix" looks to fetch html chunk associated with submissions "Data Type", "Region", "Survey", "Participant Type". The column takes the highest rank for matching columns ans sort them in ascending order relative to the sheet name "Next steps key"
* "Headers Matrix" similar to other matrix sheets it uses submissions "Data Type", "Region", "Survey", and "Participant Type" to determin which snippet or cell to fetch. This snippet is then used in the corresponsing "UXR next steps matrix" html chunk
* "Next steps key" - contains priority of various html chunks for display and or order.
* "Notes Matrix" - contains html chunk to be determined by "Participant Type", "Vendor", "Survey", "Enterprise", "default". **Please Note** there's a NotesBasic that gets added by default within the Google Apps Script Logic