function emailTextTesting() {
  var myDoc = DocumentApp.openByUrl("https://docs.google.com/document/d/1HRRHyg-2lhSu1XvCD-0ZNYsPvp_7am8lOUWtUBncIxA/edit");
  var fullDoc = myDoc.getBody();
  var fullDocText = fullDoc.getText();
  var bouncebackHtmlBeginningtag = '[BODY Response to bounceback email]';
  var pCounsel = true;
  var testFirstName = 'Mike';
  var testBadEmail = 'bonaroTest@google.com'
  
  if (pCounsel == true){
    var emailText = fullDocText.substring(fullDocText.indexOf(bouncebackHtmlBeginningtag)+(bouncebackHtmlBeginningtag.length),fullDocText.indexOf("[ENDBB1]", fullDocText.indexOf(bouncebackHtmlBeginningtag)));
    emailText = emailText + fullDocText.substring(fullDocText.indexOf("[ENDBB2]")+8,fullDocText.indexOf("[ENDBB3]", fullDocText.indexOf(bouncebackHtmlBeginningtag)));  
    emailText = emailText.replace("[IF PCOUNSEL]", '')
      .replace("[UXR FIRST NAME]", testFirstName)
      .replace("[EMAIL THAT GOT BOUNCEBACK]", testBadEmail);
  }
  else if (pCounsel == false){
    var emailText = fullDocText.substring(fullDocText.indexOf(bouncebackHtmlBeginningtag)+(bouncebackHtmlBeginningtag.length),fullDocText.indexOf("[IF PCOUNSEL]", fullDocText.indexOf(bouncebackHtmlBeginningtag)));
    emailText = emailText + fullDocText.substring(fullDocText.indexOf("[IF OTHER]")+10,fullDocText.indexOf("[ENDBB3]", fullDocText.indexOf(bouncebackHtmlBeginningtag))); 
    emailText = emailText.replace("[ENDBB2]", '')
      .replace("[UXR FIRST NAME]", testFirstName)
      .replace("[EMAIL THAT GOT BOUNCEBACK]", testBadEmail);
  }
  
  var subject = 'Oops - there’s a problem with your UX Informed Consent request';
  var message = emailText;
  var recipient = 'bonaro@google.com';
  var ccAddress = '';
  
  emailToRecipientTesting(subject, message, recipient);
}


function emailToRecipientTesting(subject, message, recipient, ccAddress){ //Generic email sending function
  var mailFromIndex = GmailApp.getAliases().indexOf('uxi-ooto-alerts@google.com'); //OOTO alias
  var mailFrom = GmailApp.getAliases()[mailFromIndex];
  
  GmailApp.sendEmail(recipient, subject, message, {from:mailFrom, cc:ccAddress, replyTo:recipient, htmlBody:message});
    Logger.log(message);
}


           /*else if ((values[i][7].indexOf(errorEmail)<0) && (values[i][8].indexOf(errorEmail)<0)){ //doing first and second checks of non-bouncebacks
            if (bouncebackStatus == ''){
              sheet.getRange(i+1, colN, 1, 1).setValue('first check complete');
            }
            else if (bouncebackStatus == 'first check complete'){
              sheet.getRange(i+1, colN, 1, 1).setValue('verified');
            }
          }*/
          
  
  
function regexTesting(){
  var line = 'Slow to Respond - Talking to Firebase Developers in India Re: Consent form for study: Personas & Workflows *PCounsel FYI only - No Action Required*';
  var subject = 'Consent form for study: Personas & Workflows *PCounsel FYI only - No Action Required*';
  var subject2 = "Invitation: CVS email sync - who's manager do we send to @ Mon Sep 9, 2019 1pm - 1:45pm (CDT) (Priya Chandrasekaran)";
  
  var s =  SpreadsheetApp.getActive();
  var sheet = s.getSheetByName("Form Responses 1");
  var values = sheet.getDataRange().getValues();
  var colP = values[0].indexOf('Email Subject Line')+1;
   
      //Logger.log('subjectLine: ' + subjectLine);
    var conversations = GmailApp.search('subject:"Re: " '+ subject2 + ' newer_than:3d');
      Logger.log('threads.length: ' + conversations.length);
    var thread;
    var message;
    for(var c=0; c<conversations.length; c++){
      thread = conversations[c];
      for(var k = 1; k<=thread.getMessageCount(); k++){ //might send 2+ emails with bounceback
      message = thread.getMessages();
        if(thread.getMessageCount() > 1){
          //for (var j=0; j<message.length; j++){
            Logger.log('c: ' + c + ' subjectLine: ' + subject2);
            Logger.log('message: ' + message[k].getFrom());
            Logger.log('k: ' + k);
          //}
        }
      }
    }
}
          
function subjectTest(){
  var subject2 = "Invitation: CVS email sync - who's manager do we send to @ Mon Sep 9, 2019 1pm - 1:45pm (CDT) (Priya Chandrasekaran)";
  //var subject2 = '[Forsale-ny] WTB: Climbing gear';
  
    
  var myDoc = DocumentApp.openByUrl("https://docs.google.com/document/d/1HRRHyg-2lhSu1XvCD-0ZNYsPvp_7am8lOUWtUBncIxA/edit");
  var fullDoc = myDoc.getBody();
  var fullDocText = fullDoc.getText();
  var oooHtmlBeginningtag = '';
  var tableEnd = fullDocText.substring(fullDocText.indexOf(oooHtmlBeginningtag)+(oooHtmlBeginningtag.length),fullDocText.indexOf("[END]", fullDocText.indexOf(oooHtmlBeginningtag)));
  
  
  var threads = GmailApp.search('subject:"Re: " ' + subject2 + ' newer_than:3d');
  //var threads = GmailApp.search('subject:' + subject2 + ' newer_than:3d');
    for (var i=0; i<threads.length; i++) {
        Logger.log('messageCount: ' + threads[i].getMessageCount())
      if (threads[i].getMessageCount() > 1){
        var messages = threads[i].getMessages();
        for (var j=0; j<messages.length; j++){
          var message = threads[i].getMessages()[j];
            Logger.log('thread: ' + i + ' message: ' + j + ' from: ' + message.getFrom());
            Logger.log('body: '+ message.getPlainBody().substr(0, message.getPlainBody().indexOf('\n> '))); //need to insert into email to requestor
            
          //if the name is in the pCounsel or other columns then email
         }//for[j] close
       }//if close
     }//for[i] close
}//function close


function subjectTest2(){ //Janice message system
  var subject2 = "Invitation: CVS email sync - who's manager do we send to @ Mon Sep 9, 2019 1pm - 1:45pm (CDT) (Priya Chandrasekaran)";
  //var subject3 = '[Forsale-ny] WTB: Climbing gear';
  
  var ss =  SpreadsheetApp.getActive();
  var s = ss.getSheetByName("Form Responses 1");
  var values = s.getDataRange().getValues();
  
  var colP = values[0].indexOf('Email Subject Line')+1;
  var colQ = values[0].indexOf('OOO Status')+1;
  var colR = values[0].indexOf('OOO Email Address')+1;
  
  for(var c=s.getLastRow()-1; c>0; c--){ //check each line of the sheet
    var subjectLine = s.getRange(c, colP, 1, 1).getValue();
    var oooStatus = s.getRange(c, colQ, 1, 1).getValue();
    var oooEmail = s.getRange(c, colR, 1, 1).getValue();
    if(subjectLine != '' && oooStatus != 'OOO'){ 
      var conversations = GmailApp.search('subject:"Re: " ' + subject2 + ' newer_than:3d'); //search based on subject line in the approved row
      //var threads = GmailApp.search('subject:' + subject3 + ' newer_than:3d');
      for (var i=0; i<conversations.length; i++) {
        var thread = conversations[i];
          //Logger.log('messageCount: ' + thread.getMessageCount())
        if (thread.getMessageCount() > 1){
          var messages = thread.getMessages();
          for (var j=0; j<messages.length; j++){
            var message = thread.getMessages()[j];
              //Logger.log('substring get from: ' + message.getFrom().substr(message.getFrom().indexOf('<')+1, message.getFrom().indexOf('@google.com')-1));
              //Logger.log('substring get from: ' + message.getFrom().split(/[<>]/));
              //Logger.log('values[c][7]: ' + values[c][7]);
            if(message.getFrom().indexOf(values[c][7])>=0){ 
                Logger.log('thread: ' + i + ' message: ' + j + ' from: ' + message.getFrom());
                Logger.log('body: '+ message.getPlainBody().substr(0, message.getPlainBody().indexOf('\n> '))); //need to insert into email to requestor
                Logger.log('values[c][7]: ' + values[c][7]);
              //emailOOOTesting(values[c][7], message.getPlainBody().substr(0, message.getPlainBody().indexOf('\n> ')));
            }//if close
          }//for[j] close
        }//if close
      }//for[i] close
    }//if close
  }//for[c] close
}//fun      
          

function emailOOOTesting(errorEmail, oooText) {
  var myDoc = DocumentApp.openByUrl("https://docs.google.com/document/d/1HRRHyg-2lhSu1XvCD-0ZNYsPvp_7am8lOUWtUBncIxA/edit");
  var fullDoc = myDoc.getBody();
  var fullDocText = fullDoc.getText();
  var oooHtmlBeginningtag = '[BODY RESPONSE TO PCOUNSEL OOO]';
  //var pCounsel = true;
  var testFirstName = 'Mike';
  //var testBadEmail = 'bonaroTest@google.com'
  
  var emailText = fullDocText.substring(fullDocText.indexOf(oooHtmlBeginningtag)+(oooHtmlBeginningtag.length),fullDocText.indexOf("[END]", fullDocText.indexOf(oooHtmlBeginningtag)));
  //emailText = emailText + fullDocText.substring(fullDocText.indexOf("[ENDBB2]")+8,fullDocText.indexOf("[ENDBB3]", fullDocText.indexOf(oooHtmlBeginningtag)));  
  emailText = emailText.replace(/\[UXR FIRST NAME\]/, testFirstName)
      .replace(/\[INSERT TEXT\]/, oooText);

  
  var subject = 'Oops - your PCounsel is OOO';
  var message = emailText;
  var recipient = 'bonaro@google.com';
  var ccAddress = '';
  
  emailToRecipient(subject, message, recipient);
}

  //message.getFrom().substr('<([^>]+)>')        
          
          
          
  /************************************/ 
  /********* ARCHIVE - IGNORE *********/
  /************************************/

function getBusinessDays(numDays, startDate) {
  var holidayCalendarID = "en.usa#holiday@group.v.calendar.google.com";
  var holidayCalendar = CalendarApp.getCalendarById(holidayCalendarID);
 
  var tempDay = startDate;
   
  var events = [];
  
  for(var i=0; i<numDays; i++){
    tempDay.setDate(tempDay.getDate()+1)
    events = holidayCalendar.getEventsForDay(tempDay);
   
    while((events.length>0) || (tempDay.getDay() == 0) || (tempDay.getDay() == 6)){
      tempDay.setDate(tempDay.getDate()+1);
      events = holidayCalendar.getEventsForDay(tempDay);
    }
   
  }
  var formattedDate = Utilities.formatDate(tempDay, "PST", "MMMM dd, YYYY");
  return formattedDate;
  
}

function moveLinksFromOldSheet() {
  
  var oldSheet = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1Smf13H7Lma5AjvqqEYD43_MXERIv89f2V-aCBh_hTCg/edit#gid=638343778");
  var newSheet = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1cdesVloJ3yqrqvQEU0Nv1jW8Eb-2VWD6ZoyYR-oj0Pw/edit#gid=1092308988");
  var linksTab = newSheet.getSheetByName("Storing Powerform Links");
  var linksKey = linksTab.getRange(2, 2, 27, 2).getValues();
  var linksValidation = linksTab.getRange(2, 2, 27, 2).getDataValidations();
  
  
  
  var consentKey = 
      [["External Adults","Basic", 39, 92],
       ["External Adults","PII Only", 40, 93],
       ["External Adults","Video", 41, 94],
       ["External Adults","SPII Only", 43, 96],
       ["External Adults","SPII Video", 45, 98],
       ["External Adults","Bio Video SPII", 51, 104],
       ["External Adults","Bio SPII (no video)", 53, 106],
       ["External Adults","Bio Video (no SPII)", 49, 102],
       ["External Adults","Bio Only", 47, 100],
       ["Minors","Basic", 5 , 58],
       ["Minors","PII Only", 6, 59],
       ["Minors","Video", 7, 60],
       ["Minors","SPII Only",9, 62 ],
       ["Minors","SPII Video",11, 64],
       ["Minors","Bio Video SPII", 17, 70],
       ["Minors","Bio SPII (no video)",19, 72],
       ["Minors","Bio Video (no SPII)", 15, 68],
       ["Minors","Bio Only", 13, 66],
       ["Googlers","Basic", 22, 75],
       ["Googlers","PII Only", 23, 76],
       ["Googlers","Video", 24, 77],
       ["Googlers","SPII Only", 26, 79],
       ["Googlers","SPII Video", 28, 81],
       ["Googlers","Bio Video SPII", 34, 87],
       ["Googlers","Bio SPII (no video)", 36, 89],
       ["Googlers","Bio Video (no SPII)", 32, 85],
       ["Googlers","Bio Only", 30, 83]];
  
  
  var tabs = oldSheet.getSheets();
  var loc = 0;
  var country;
  var lastrow;
  for (var t = 50; t<tabs.length; t++){
    loc = tabs[t].getName().indexOf("_Key"); 
    if (loc>0){
      country = tabs[t].getName().substr(0,loc);
      lastrow=linksTab.getLastRow()+1;
      
      linksTab.getRange(lastrow, 2, 27, 2).setDataValidations(linksValidation);
      linksTab.getRange(lastrow, 2, 27, 2).setValues(linksKey);
      linksTab.getRange(lastrow, 1, 27, 1).setValue(country);
      
      for(var i=0; i<27; i++){
        for(var j=0; j<consentKey.length; j++){
          if((linksKey[i][0] == consentKey[j][0]) && (linksKey[i][1] == consentKey[j][1])){
            linksTab.getRange(lastrow+i, 5, 1, 1).setValue(tabs[t].getRange(consentKey[j][2], 13).getValue());
            linksTab.getRange(lastrow+i, 4, 1, 1).setValue(tabs[t].getRange(consentKey[j][3], 13).getValue());
          }
        }
      }
      
    }
  }
  
}



