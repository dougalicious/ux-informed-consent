/* OWNER: douglascox@, uxi-automation
 * SOURCES: UX Informed Consent
 *
 * DATE:
 * DESCRIPTION: takes event object from form submission send automated information with related material to form submission
 * 
 * @param {(formEvent)} -description ie takes a number
 * @return null
 */
/*
EXTERNAL ADULT ONLY: Do any of these circumstances apply to your study? If so, you will receive an Enterprise consent form.	

GOOGLER VERSION: Do you expect that most or all of your Googler participants will dial into the study from their homes and do you need to record video of the Googler's face/home space? Note for Janice - this should only show up if they select Googlers + video collected

*/
function depricatedOnformsubmit() {
  function __onFormSubmit(e) {

    Logger.log("start --onformsubmit")
    e = e || artificalEvent();
    /********* Sync with Current sheet *********/

    const sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Form Responses 2")
    const activeRow = e.range.getRow();
    // match all form questions to variable names using our key
    const key = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("key");

    const keyDataBySheet = getDataBySheet(key)
    const activeDataBySheet = getDataBySheet(sheet);
    const getKeyDataMap = dataMapConverter(keyDataBySheet.body)
    /************************************/
    /********* DEFINE VARIABLES *********/
    /************************************/
    const vendorQuestion = "Will you be working with a vendor?"
    const studyParticipantsQuestion = "Who are your study participants?"
    const enterpriseQuestion = "EXTERNAL ADULT ONLY: Do any of these circumstances apply to your study? If so, you will receive an Enterprise consent form."
    const isEnterprise = (response) => !(response.includes("None of the above") || response.includes("None of these apply") || response === "" || /google/gi.test(e.namedValues[studyParticipantsQuestion][0]));
    const regionByCountry = getRegionByCountry()
    const formQuestions = {
      emailAddress: e.namedValues["Email Address"][0],
      isEnterprise: isEnterprise(e.namedValues[enterpriseQuestion][0]),
      powerFormType: e.namedValues.hasOwnProperty(enterpriseQuestion) ? "Enterprise Powerform" : "Consumer Powerform(s)",
      countryHeaders: [
        "In which countries will the participants be located? ",
      ],
      selectedDataTypes: e.namedValues["What type of data are you collecting in the study? (Check all that apply)"][0],
      selectedParticipantTypes: e.namedValues["Who are your study participants?"][0],
      getVendorNotes: e.namedValues[vendorQuestion][0] === "Yes" ? vendorQuestion : "",
      getEnterpriseNotes: (isEnterprise(e.namedValues[enterpriseQuestion][0]) ? enterpriseQuestion : ""),
      pCounselEmail: e.namedValues["Who is your PCounsel? Enter the full email address (ldap@google.com)."][0],
      additionalCC: e.namedValues.hasOwnProperty("Who else should be cc'd on the email? (ldap@google.com, ldap@google.com)") ? e.namedValues["Who else should be cc'd on the email? (ldap@google.com, ldap@google.com)"][0] : ""
    }

    const [countryCell] = keyDataBySheet.body.filter(rowAsObj => rowAsObj["Script Variable Name"] === "countries")
      .map(prop => prop["Form Text"])
    const hasCountry = (acc, next) => /^\s*$/.test(e.namedValues[next]) ? acc : acc.concat(e.namedValues[next])
    const selectedCountries = Object.keys(e.namedValues)
      .filter(header => formQuestions.countryHeaders.includes(header))
      .map(str => str.includes(',') ? str.split(',').map(str => str.trim()) : str)
      .reduce(hasCountry, [])

    const activeRowAsObj = activeDataBySheet.body[activeRow - 2]; // -2 to account for header and zero base index
    const countriesConcatRange = activeRowAsObj.getRangeByColHeader(countryCell)
    // update column countries concat
    countriesConcatRange.setValue(selectedCountries)

    const emailMapR = keyDataBySheet.body.reduce(collectEmailMap, {})

    // [emailMapR.Countries] = emailMapR.countries
    emailMapR.requestor = formQuestions.emailAddress
    emailMapR.Enterprise = formQuestions.isEnterprise
    activeRowAsObj.getRangeByColHeader('Enterprise Yes/No').setValue(emailMapR.Enterprise)

    emailMapR.powerFormType = formQuestions.powerFormType
    //data types  getKeyDataMap
    // have it reference "key Tab"
    const dataTypeFieldOnFrom = getKeyDataMap("dataTypes", "Form Text")
    const [pdd] = e.namedValues[getKeyDataMap("pdd", "Form Text")]
    emailMapR.pdd = pdd;
    /************************************/
    /********* GET LIST OF URLS *********/
    /************************************/
    const urlSheet = SpreadsheetApp.getActive().getSheetByName("Storing Powerform Links");
    const { body: powerFormLinks } = getDataBySheet(urlSheet)
    const countryList = selectedCountries[0].split(',').map(country => country.trim())
    emailMapR["countries"] = countryList
    // fn countryMatchByRegex helper function in utilities
    const getListCountryLanguagePowerFormMatch = (acc, countryLanguage) => acc.concat(powerFormLinks.filter(rowAsObj => {
      return countryMatchByRegex(countryLanguage, rowAsObj["Country+Language"])
    }))
    const otherCountryLanguageLinksMatch = powerFormLinks.filter(props => props["Country+Language"] === "Other");
    const listCountryLanguagePowerFormMatch0 = emailMapR["countries"].reduce(getListCountryLanguagePowerFormMatch, [])
    const listCountryLanguagePowerFormMatch = emailMapR["countries"].reduce((acc, next) => {
      let powerformap = powerFormLinks.filter(props => props["Country+Language"] === next)
      const matchCountryPowerForm = powerformap.length !== 0
      // if(!matchCountryPowerForm) {debugger}
      let list = matchCountryPowerForm ?
        powerformap :
        otherCountryLanguageLinksMatch.map(props => {
          let copyOfOtherPowerFormObj = Object.assign({}, props)

          copyOfOtherPowerFormObj["Country+Language"] = next;

          return copyOfOtherPowerFormObj
        })
      //  if(!matchCountryPowerForm) {debugger}
      const c = [...acc, ...list]

      return c
    }, [])

    const countryLanaguageLinksMatch = listCountryLanguagePowerFormMatch.length !== 0 ? listCountryLanguagePowerFormMatch : otherCountryLanguageLinksMatch

    const { body: dataTypesPerm } = getDataBySheet(SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Data Types Permutations"))
    //emailMap["Localized"] = "Yes";
    const selectedDataTypes = getSelectedDataTypes(formQuestions.selectedDataTypes, keyDataBySheet.body).map(props => props["Script Variable Name"])

    const participantTypes = getSelectedDataTypes(formQuestions.selectedParticipantTypes, keyDataBySheet.body)
    const selectedParticipantTypes = participantTypes.map(props => props["Script Variable Name"])
    const hasVendor = e.namedValues[vendorQuestion]
    let hasVendorNotes = formQuestions.getVendorNotes
    let hasEnterpriseNotes = formQuestions.getEnterpriseNotes
    const notesCollection = [formQuestions.selectedParticipantTypes, formQuestions.getEnterpriseNotes, formQuestions.getVendorNotes].filter(prop => prop !== "").join(', ')
    const selectedNotesTypes = getSelectedDataTypes(notesCollection, keyDataBySheet.body).map(props => props["Notes"]).filter(prop => prop !== "");


    // adding the capture of Video + Googler + "Yes"@videoAtHome
    const hasVideoDataType = selectedDataTypes.includes('video');
    const hasGooglerVideoCollected = emailMapR.videoAtHome === "Yes"
    const hasVideoAtGooglersHome = (hasVideoDataType && hasGooglerVideoCollected)
    emailMapR.videoAtHome = hasVideoAtGooglersHome
    hasVideoAtGooglersHome && selectedDataTypes.push('videoAtHome')
    const uxrNextSteps = {
      dataTypes: selectedDataTypes,
      participantTypes: selectedParticipantTypes
    }

    let isMatchDataType = countryLanaguageLinksMatch.filter(isDataTypeMatch)
    let isMatchParticipantType = countryLanaguageLinksMatch.filter(isParticipantTypeMatch)

    let powerFormsUrls = countryLanaguageLinksMatch.filter(filterByParticipantDataTypes)

    if (powerFormsUrls.length === 0) {
      throw ("No powerform links captured")
      // powerFormsUrls = countryLanaguageLinksMatch.filter(isDataTypeMatch).filter( (props) => props.Participant.includes("External") )
    }

    // emailMapR["Custom Required"] = powerFormsUrls.some(props => {
    //   let powerFormTypeCol = emailMapR.powerFormType
    //   let regexTest = (str) => {
    //     let regex = new RegExp(str, "gi")
    //     return function (cellText) { return regex.test(cellText) }
    //   }
    //   let isPowerFormMatch = regexTest(powerFormTypeCol)
    //   return ["Custom form needed", "NEEDS TO BE CREATED", "Custom form needed - country not yet localized"].some(cellText => isPowerFormMatch(cellText))
    // }) ? "Yes" : "No"
    emailMapR["powerFormsUrlsMatch"] = powerFormsUrls

    if (emailMapR.Enterprise) {
      Array.isArray(emailMapR.participantTypes) ? emailMapR.participantTypes.push("Enterprise") : emailMapR.participantTypes = [emailMapR.participantTypes, "Enterprise"]
    }

    //powerFormsUrls
    const nextSteps = getNextSteps(emailMapR, uxrNextSteps)
    // add moderator type
    emailMapR["otherCC"] = formQuestions.pCounselEmail.trim().split(/[\s,]+/g)
      .reduce((acc, next) => next === "" ? acc : acc.concat(next), []);
    emailMapR["additionalCC"] = formQuestions.additionalCC

    const params = Object.assign(emailMapR, nextSteps, { "selectedDataTypes": selectedDataTypes, "selectedParticipantTypes": selectedParticipantTypes, getKeyDataMap, ss: sheet.getParent(), activeRowAsObj })
      ;

    let emailParams = getEmailParams(params);
    debugger
    try {
      sendEmail(emailParams, emailMapR)
      activeRowAsObj.getRangeByColHeader('Email Sent').setValue(new Date());
    } catch (e) {
      Logger.log('email not sent')
      Logger.log(e)
      activeRowAsObj.getRangeByColHeader('Email Sent').setValue(`false: ${e}`);
    }
    /********************************/
    /********** Helper fns **********/
    /********************************/

    function dataMapConverter(arr) {
      return function (variableName, type) {
        const [objByRow] = arr.filter(rowAsObj => rowAsObj["Script Variable Name"] === variableName) || [null]
        return objByRow[type]
      }
    }

    function getSelectedDataTypes(str, body) {
      var dataTypesAsArr = body.filter(props => {
        let containsDataType = str.includes(props["Form Text"])

        return containsDataType
      })

      return dataTypesAsArr
    }

    /* OWNER: douglascox, uxi-automation
   * SOURCES: UX Informed Consent
   *
   * DATE: 2020.08.25
   * DESCRIPTION: accumulator helper function
   * Converts form answers to "Script Variables Name" relative to key tab
   *        + seperates dataTypes selected into an Array
   *        + adds moderator Type And Vendor
   * @param {(acc|Array)} -description ie takes a number
   * @param {(next|Object)} -description ie takes a number
   * @return {(Object)} - key: "Script Varaible Name" , value: "Form Text"
   */
    function collectEmailMap(acc, next) {
      let fromForm = next["Form Text"]
      let svn = next["Script Variable Name"]
      let obj = {}
      let value = e.namedValues.hasOwnProperty(fromForm) ? e.namedValues[fromForm][0] : []
      value = value.indexOf(',') === -1 ? value : value.split(',').map(prop => prop.trim())
      svn === "vendor" && isVendor(obj, svn, value === "Yes")

      if (svn === 'dataTypes') {
        const allavailabledataType = getAvailableDataTypes()

        const availableDataTypes = getAvailableDataTypes().filter(dataTypeAsStr => e.namedValues[fromForm][0].includes(dataTypeAsStr))
        value = availableDataTypes
      }
      obj[svn] = value

      return e.namedValues.hasOwnProperty(fromForm) ? Object.assign(acc, obj) : acc

      function isVendor(o, svn, conditional) {
        return conditional ? addModeratorType(o, { moderatorType: "Third Party Vendor", svn: "Yes" }) : addModeratorType(o, { moderatorType: "Googler or Google TVC", svn: "No" })
        function addModeratorType(prop, obj) {
          return Object.assign(prop, obj)
        }

      }
      function getAvailableDataTypes() {
        let lastDataTypeIdx = keyDataBySheet.body.findIndex(props => props["Script Variable Name"] === "dataTypesNone")

        return keyDataBySheet.body.slice(0, lastDataTypeIdx + 1)
          .filter(props => props["Script Variable Name"] !== "videoAtHome")
          .map(props => props["Form Text"])
      }
    }

    // filters for match on dataTypes and ParticipantTypes
    /* OWNER: douglascox, uxi-automation
     * SOURCES: UX Informed Consent
     *
     * DATE: 2020.08.25
     * DESCRIPTION: Grabs data from Sheet "Storing PowerForm Links" that match criterias of DataTypes && participant types
     * INPUT: includes dataBySheet of matching country+Language
     * @param {(object|rowAsObj)} - rowAsObj each row converted to object here header is key, and value is realtive to that cell for the row
     * @return {(Boolean)} - filters through to grab only those rows that MATCH Participant AND Data Type 
     */
    function isParticipantTypeMatch(props) {
      let tvcIsExternal
      if (selectedParticipantTypes.includes("TVCs")) {
        tvcIsExternal = "External Adults"
      }
      //debugger
      return [...selectedParticipantTypes, tvcIsExternal].includes(props["Participant"]);
    }
    function isDataTypeMatch(props) {
      let [dataTypeMatch] = dataTypesPerm.filter(findDataTypeMatch)

      return (props.Data === dataTypeMatch.Key)


      function findDataTypeMatch(props) {
        let dataTypeFromKey = keyDataBySheet.body.filter(props => props["Data Type"] !== "").reduce((acc, next, idx, arr) => {
          return surveyContainsDataTypeFromKey(next) ? acc.concat(next) : acc;

          function surveyContainsDataTypeFromKey(props) {

            let formText = props["Form Text"]
            let regex = new RegExp(formText, "gi")
            let strOfDataTypes = Array.isArray(emailMapR.dataTypes) ? emailMapR.dataTypes.join(" ") : emailMapR.dataTypes
            let boo = strOfDataTypes.includes(formText)

            return boo
          }
        }, []);
        let dataTypes = dataTypeFromKey.map(props => props["Data Type"])
          .reduce((acc, next) => acc.includes(next) ? acc : acc.concat(next), [])
        let ans = dataTypes

        let res = Object.keys(props).slice(1).every(key => {
          let a = dataTypes.includes(key)
          let b = (props[key] === "Yes")
          let c = props[key] === "No"
          let d = dataTypes.includes(key) ? (props[key] === "Yes") : props[key] === "No"

          return dataTypes.includes(key) ? (props[key] === "Yes") : props[key] === "No"
        });
        // returns dtp key name
        return res
      }
    }
    function filterByParticipantDataTypes(props) {
      let isDataType = isDataTypeMatch(props)
      let isPartipantType = isParticipantTypeMatch(props)
      return isDataType && isPartipantType
    }

  }
  function getCCEmails({ selectedDataTypes, selectedParticipantTypes }) {
    const sheetName = "Who to cc"
    const ss = SpreadsheetApp.getActiveSpreadsheet()
    const sheet = ss.getSheetByName(sheetName)
    const { body } = getDataBySheet(sheet);
    return body.filter(hasEitherParticipantANDORDataTypes)
      .map(props => props["Who to cc"])
      .reduce((acc, next) => acc.includes(next) ? acc : acc.concat(next), [])
    // .join(", ");

    function hasEitherParticipantANDORDataTypes(props) {
      return hasValidType(selectedParticipantTypes, "Participant Type")(props) && hasValidType(selectedDataTypes, "Data Type")(props)
    }

    function hasValidType(arr, str) {
      return function (props) {
        return arr.includes(props[str])
      }
    }
  }
  /* OWNER: douglascox, uxi-automation
   * SOURCES: UX Informed Consent
   *
   * DATE: 2020.08.25
   * DESCRIPTION: Sends out email relative to form submission
   * 
   * @param {(subject|string)} - the subject of the email
   * @param {(htmlBody|string)} - html body of the email
   * @param {(emailMapR|object)} - dervived state information
   * @return undefined
   */
  // function sendEmail({ subject, htmlBody }, emailMapR) {

  //   let { otherCC: pcounselEmail, "requestor": recipient, selectedParticipantTypes, selectedDataTypes, additionalCC } = emailMapR

  //   const fromWhoToCCSheet = getCCEmails({ selectedParticipantTypes, selectedDataTypes })
  //   debugger
  //   /******** add cc's ********* */
  //   const pcounselEmailAsStr = Array.isArray(pcounselEmail) ? pcounselEmail.join(", ") : pcounselEmail
  //   const ccList = [pcounselEmailAsStr, ...fromWhoToCCSheet, "uxr-ops-coordinators@google.com"]
  //   const cc = additionalCC === "" ? ccList.join(', ') : ccList.concat(additionalCC).join(', ')
  //   // const cc = [pcounselEmailAsStr, ...fromWhoToCCSheet, "uxr-ops-coordinators@google.com"].join(',')

  //   const ccDevTest = [pcounselEmailAsStr].join(', ')

  //   const options = {
  //     cc: cc,
  //     from: getAlias("ux-consent@google.com"),
  //     htmlBody: htmlBody,
  //     // replyTo: "ux-consent@google.com"
  //   }
  //   debugger
  //   // GmailApp.sendEmail('douglascox@google.com', subject, "default body text", options)
  //   SpreadsheetApp.getActiveSpreadsheet().toast("send email commented out")
  //   // GmailApp.sendEmail(recipient, subject, "default body text", options)
  //   //  GmailApp.sendEmail(recipient, subject , "default body text", options)
  //   Logger.log(`email sent`)

  //   function getAlias(alias) {
  //     const aliasList = GmailApp.getAliases();
  //     const aliasIdx = aliasList.indexOf(alias)
  //     const hasAlias = aliasIdx !== -1
  //     return hasAlias ? aliasList[aliasIdx] : null
  //   }
  // }

  /* OWNER: douglascox, uxi-automation
   * SOURCES: UX Informed Consent
   *
   * DATE: 2020.08.25
   * DESCRIPTION: for UXR, Pcounsel, Header, Notes grabs corresponding data from Sheet Matrix
   *         + sorts relative to Next Step Type
   * @param {(props|obj)} - emailMapR 
   * @param {(uxrNextStepsProps|string)} - Object containing Participant Type and DataType information
   * @return {object} - next Next
   */
  // function getNextSteps(props, uxrNextStepsProps) {
  //   const ss = SpreadsheetApp.getActiveSpreadsheet()
  //   const { body: regionsMap } = getDataBySheet(ss.getSheetByName("Region Map"))
  //   const allRegionsDefault = regionsMap.reduce((acc, next) => acc.includes(next.Region) ? acc : acc.concat(next.Region), [])
  //   let regions = getRegions(props).map(props => props["Region"])
  //     .reduce((acc, next) => acc.includes(next) ? acc : [...acc, next], [])
  //   regions = regions.length === 0 ? allRegionsDefault : regions
  //   const uxrNextSteps = getUXRnextSteps(props, uxrNextStepsProps, regions, ss.getSheetByName("UXR next steps matrix")) // gets consoldiate futuer down @convertUxrNextStepsToHtml  .reduce(removeDuplicates, []);

  //   const pCounselNextSteps = getPCounselNextSteps(props, uxrNextStepsProps, regions, ss.getSheetByName("PCounsel next steps matrix"))//.reduce(removeDuplicates, [])
  //   const headerNextSteps = getUXRnextSteps(props, uxrNextStepsProps, regions, ss.getSheetByName("Headers matrix"))//.reduce(removeDuplicates, [])
  //   const notesMatrix = getNotesNextSteps(props, uxrNextStepsProps, regions, ss.getSheetByName("Notes matrix"), headerNextSteps)//.reduce(removeDuplicates, [])

  //   let uxrcells = uxrNextSteps.map(props => props.cell)

  //   return {
  //     uxrNextSteps,
  //     pCounselNextSteps,
  //     headerNextSteps,
  //     notesMatrix
  //   }
  //   function removeDuplicates(acc, next) {
  //     let hasObject = acc.map(o => o.cell).includes(next.cell)
  //     let a = [...acc, next]
  //     return hasObject ? acc : acc.concat(next)
  //   }



  //   // collecting all notes, NOT by column like uxr next steps and pcounsel
  //   function getNotesNextSteps(props, { dataTypes, participantTypes }, regions, sheet, headerNextSteps) {
  //     const values = sheet.getRange(1, 1, 5, 5).getValues()

  //     const valuesMatrix = (values.slice(1)).map(grabColumMatches)
  //       .reduce((acc, next) => acc.concat(next), []) //flatten
  //     const isSurvey = props.survey === "Yes"
  //     const isEnterprise = props.Enterprise
  //     const isVendor = props.vendor === "Yes"
  //     const sortNotes = sortByNotes();


  //     const result = valuesMatrix.reduce(extractNotes, [])
  //       .reduce(removeDuplicates, [])
  //       .sort(sortNotes)
  //     // General notesSurvey prepended to always be there
  //     result.unshift({ cell: 'NotesBasic' })


  //     return result
  //     function extractNotes(acc, next, idx, arr) {
  //       let { cell, top, left } = next;
  //       let isValidParticipantType = participantTypes.includes(top)
  //       let isValid = isValidVendorSurveyEnterprise(left)
  //       let hasNotesInAccumulator = acc.includes(cell)
  //       let isDefaultAndBlank = (left === "default" && isValidParticipantType && cell !== "" && !hasNotesInAccumulator)
  //       if ((isValid && isValidParticipantType && cell !== "") || (isDefaultAndBlank)) {
  //         let props = next.cell.split(',').map(str => str.trim())
  //         return acc.concat(props.map(val => Object.assign(next, { cell: val })))
  //       } else {
  //         return acc
  //       }
  //     }

  //     function isValidVendorSurveyEnterprise(str) {
  //       return {
  //         Vendor: isVendor,
  //         Survey: isSurvey,
  //         Enterprise: isEnterprise,
  //         null: null
  //       }[str]
  //     }


  //     function grabColumMatches(row, rowIdx, arr) {
  //       return row.slice(1).map((cell, colIdx) => {
  //         return {
  //           cell: cell,
  //           left: values[rowIdx + 1][0],
  //           top: values[0][colIdx + 1]
  //         }
  //       })
  //     }
  //     function sortByNotes() {
  //       var range = sheet.getRange(8, 1, 6, 2)
  //       var values = range.getValues()
  //       var sortKey = values.reduce((acc, next) => {
  //         let [name, num] = next
  //         acc[name] = Number(num)
  //         return acc;
  //       }, {})
  //       return (left, right) => {
  //         //get ranking value
  //         let leftValue = sortKey[left.cell] || Number.NEGATIVE_INFINITY
  //         let rightValue = sortKey[right.cell] || Number.NEGATIVE_INFINITY

  //         return leftValue < rightValue ? 1 : -1
  //       }
  //     }
  //   }


  //   function getPCounselNextSteps({ }, { dataTypes, participantTypes }, regions, sheet) {
  //     let dataRange = sheet.getDataRange()
  //     let values = dataRange.getValues()
  //     var body = values.slice(2).map(rows => rows.slice(1))
  //     const getDataType = (rowIdx) => values[rowIdx + 2][0]
  //     const getRegion = (colIdx) => values[0][1 + colIdx] || []
  //     const getParticipantType = (colIdx) => values[1][1 + colIdx]
  //     const filterByPointMatchType = initFilterByPointMatchType({ participantTypes, regions, dataTypes, survey: null })
  //     const grabColumMatches = initGrabColumnMatchec({
  //       regionFromMatrix: getRegion,
  //       participantTypeFromMatrix: getParticipantType,
  //       dataTypeFromMatrix: getDataType,
  //       isSurveyFromMatrix: null
  //     })
  //     let transposeMatrix = transpose(body).map((col, colIdx) => grabColumMatches(col, colIdx))
  //     const pCounselNextSteps = transposeMatrix.map(byColumn => byColumn.filter(filterByPointMatchType))
  //     const sortNextStepsFn = sortByNextStepsKey()
  //     const matchNextStepsByCol = pCounselNextSteps
  //       .filter(arr => arr.length !== 0)
  //       .map(arr => arr.sort(sortNextStepsFn))
  //       .map(arr => arr.length > 1 ? arr[0] : arr)

  //     const [highestRanking, ...otherPcounsel] = matchNextStepsByCol.sort(sortNextStepsFn)

  //     return Array.isArray(highestRanking) ? highestRanking : [highestRanking]

  //   }

  //   function getUXRnextSteps({ survey }, { dataTypes, participantTypes }, regions, sheet) {
  //     const isSurvey = survey === "Yes" ? true : false;
  //     const sheetName = sheet.getName()
  //     const isHeaderMatrix = sheetName === "Headers matrix"
  //     let dataRange = sheet.getDataRange()
  //     let values = dataRange.getValues()
  //     var body = values.slice(3).map(rows => rows.slice(1))

  //     const getDataType = (rowIdx) => values[rowIdx + 3][0]
  //     const getRegion = (colIdx) => values[0][1 + colIdx] || []
  //     const getSurvey = (colIdx) => values[1][1 + colIdx]
  //     const getParticipantType = (colIdx) => values[2][1 + colIdx]
  //     const grabColumMatches = initGrabColumnMatchec({
  //       isSurveyFromMatrix: getSurvey,
  //       regionFromMatrix: getRegion,
  //       participantTypeFromMatrix: getParticipantType,
  //       dataTypeFromMatrix: getDataType
  //     })
  //     const filterByPointMatchType = initFilterByPointMatchType({ participantTypes, regions, dataTypes, survey })
  //     let transposeMatrix = transpose(body).map((col, colIdx) => grabColumMatches(col, colIdx))
  //     let { cell: cellValueDefault } = transposeMatrix[0][0]

  //     let defaultMatrixValue = { cell: cellValueDefault, isSurveyFromMatrix: isSurvey }
  //     const uxrNextSteps = transposeMatrix.map(byColumn => byColumn.filter(filterByPointMatchType))
  //     const sortNextStepsFn = sortByNextStepsKey()

  //     const matchNextSteps = uxrNextSteps
  //       .filter(arr => arr.length !== 0)
  //       .map(arr => arr.sort(sortNextStepsFn))
  //       .map(arr => arr[0])
  //       .reduce((acc, next) => {
  //         if (isHeaderMatrix) {
  //           return acc.concat(Object.assign({}, next, { cell: next.cell }))
  //         } else if (next.cell.includes(',') && !isHeaderMatrix) {
  //           let types = next.cell.split(',').map(str => str.trim())

  //           return acc.concat(types.map(str => Object.assign({}, next, { cell: str })))
  //         } else {
  //           return acc.concat(next)
  //         }
  //       }, []).sort(sortNextStepsFn);

  //     let matchNextStepsWithDefault = matchNextSteps.length === 0 ? matchNextSteps.concat(defaultMatrixValue) : matchNextSteps;

  //     return matchNextStepsWithDefault;

  //   }
  //   function transpose(a) {
  //     return Object.keys(a[0]).map(function (c) {
  //       return a.map(function (r) { return r[c]; });
  //     });
  //   }
  //   function initFilterByPointMatchType({ participantTypes, regions, dataTypes, survey }) {
  //     // filter UXR next steps by isSurvey, region, participantType, dataType
  //     return function filterByPointMatchType({ isSurveyFromMatrix, regionFromMatrix, participantTypeFromMatrix, dataTypeFromMatrix }) {
  //       let isParticipantTypeMatch = participantTypes.some(participantType => participantType.includes(participantTypeFromMatrix))
  //       let isRegionMatch = regions.some(region => regionFromMatrix.includes(region))
  //       let isDataTypateMatch = dataTypes.some(dataType => dataType === dataTypeFromMatrix)
  //       return ((!!survey ? isSurveyFromMatrix === survey : true)
  //         && isParticipantTypeMatch
  //         && isRegionMatch
  //         && isDataTypateMatch
  //       )
  //     };
  //   }
  //   function initGrabColumnMatchec({ isSurveyFromMatrix, regionFromMatrix, participantTypeFromMatrix, dataTypeFromMatrix }) {
  //     // const headerNextSteps  = getUXRnextSteps(props, uxrNextStepsProps, regions, ss.getSheetByName("Headers matrix"))
  //     return function grabColumMatches(col, colIdx) {

  //       return col.map((cell, rowIdx) => {
  //         const result = {
  //           cell: cell,
  //           isSurveyFromMatrix: (isSurveyFromMatrix !== null ? isSurveyFromMatrix(colIdx) : null),
  //           regionFromMatrix: regionFromMatrix(colIdx),
  //           participantTypeFromMatrix: participantTypeFromMatrix(colIdx),
  //           dataTypeFromMatrix: dataTypeFromMatrix(rowIdx),
  //           //   header: headerNextSteps.find(participantTypeFromMatrix(colIdx))
  //         }
  //         return result
  //       })
  //     }
  //   }
  //   function sortByNextStepsKey() {
  //     const sheetName = "Next steps key"
  //     let sheet = ss.getSheetByName(sheetName)
  //     sheet === null && (() => { throw (`unable to find sheet named ${sheetName}`) })
  //     let { body: sortKey } = getDataBySheet(sheet)
  //     return (left, right) => {
  //       //get ranking value
  //       let leftValue = getRanking(left.cell) || Number.NEGATIVE_INFINITY
  //       let rightValue = getRanking(right.cell) || Number.NEGATIVE_INFINITY

  //       return leftValue < rightValue ? 1 : -1
  //       function getRanking(str) {
  //         let [num] = sortKey.filter(props => props["Next Step"] === str)
  //           .map(props => props["Ranking"])

  //         return num
  //       }
  //     }
  //   }

  //   function getRegions(props) {
  //     const { countries } = props;
  //     return regionsMap.filter((next, idx) => {
  //       const regionsMapCountry = next["Country"]
  //       return countries.some((country) => countryMatchByRegex(country, regionsMapCountry))
  //     })
  //   }



  // }

  /* OWNER: douglascox, uxi-automation
   * SOURCES: UX Informed Consent
   *
   * DATE: 2020.08.25
   * DESCRIPTION: generates email html template from form responsed.
   * @param {(props|obj)} - emailMapR -> gathered state data
   * @return {subject:string} - derived subject
   * @return {htmlBody: string} - html email template
   */
  // function getEmailParams(emailMapR) {
  //   const ss = SpreadsheetApp.getActiveSpreadsheet()

  //   // fetch html templates
  //   // dev
  //   // const myDoc = DocumentApp.openByUrl("https://docs.google.com/document/d/1jKvfwu1a_b5Meo_DJKwgEWaeZM7raptIwm8U23Zh3yY/edit");
  //   // prod
  //   const myDoc = DocumentApp.openByUrl("https://docs.google.com/document/d/1QBVhWKtlA95luujmz77_XHH5OSpppZ-KsbfPsK3HxZ4/edit")
  //   const fullDoc = myDoc.getBody();
  //   const fullDocText = fullDoc.getText();
  //   const { uxrNextSteps, pCounselNextSteps, headerNextSteps, notesMatrix, studyName, getKeyDataMap, powerFormsUrlsMatch, activeRowAsObj } = emailMapR;

  //   const subject = getSubjectTemplateHolder(pCounselNextSteps);
  //   activeRowAsObj.getRangeByColHeader("Email Subject Line").setValue(subject)
  //   const getChunkByPlaceHolder = getHtmlChunk(fullDocText);
  //   const htmlTemplates = getChunkByPlaceHolder(`[BODY UXR-facing]`);

  //   const body = addTextToBody(htmlTemplates);

  //   return { subject, htmlBody: body };

  //   // determines Subject relative to FYI||Action pcounsel
  //   function getSubjectTemplateHolder(pCounselNextSteps) {
  //     const nextStepsKeySheet = ss.getSheetByName("Next steps key")
  //     const { body: sheetProps } = getDataBySheet(nextStepsKeySheet)
  //     const actionKey = sheetProps.reduce((acc, next) => {
  //       let { "Next Step": pCounselNextStep, Subject: subjectType } = next;
  //       if (subjectType === "") { return acc }
  //       acc.hasOwnProperty(subjectType) ? acc[subjectType].push(pCounselNextStep) : acc[subjectType] = [pCounselNextStep]
  //       return acc;
  //     }, {})

  //     const detailActions = actionKey["actionrequired"]
  //     const isActionRequired = pCounselNextSteps.map(props => props.cell).some(pNext => detailActions.includes(pNext))
  //     const pCounselType = isActionRequired ? `ACTION REQUIRED` : `FYI ONLY`
  //     return `Consent forms for ${studyName} | PCounsel ${pCounselType}`
  //   }
  //   /* OWNER: douglascox, uxi-automation
  //    * SOURCES: UX Informed Consent
  //    *
  //    * DATE: 2020.08.25
  //    * DESCRIPTION: generates email html template from form responsed.
  //    * @param {(body|String)} - general html chunk [Body UXR-facing]
  //    * @return {subject:string} - derived subject
  //    * @return {htmlBody: string} - html email template
  //    */
  //   function addTextToBody(body) {
  //     const { requestor, studyName, otherCC: pCounselEmails, productName, pCounselNextSteps: pcounselActionDetails, tldr, moderatorType, participantTypes, selectedDataTypes, countries: countriesRequested, pdd, uxrNextSteps, notesMatrix: notesNextSteps, displayLinks }
  //       = emailMapR
  //     const isActionRequired = Array.isArray(pcounselActionDetails) ? pcounselActionDetails.every(pcounselAction => /fyi/gi.test(pcounselAction)) ? false : true
  //       : /fyi/gi.test(pcounselActionDetails)
  //     let pcounselAction = isActionRequired ? pcounselActionDetails.filter(str => !/fyi/gi.test(str)) : pcounselActionDetails
  //     const formatPcounselNames = (arr) => arr.length === 1 ? `${arr[0]}` : arr.slice(0, -1).join(", ").concat(` and ${arr.slice(-1)[0]}`)

  //     const pcounselNamesAsArr = Array.isArray(pCounselEmails) ? (pCounselEmails.map(getNameFromLdap)) : getNameFromLdap(pCounselEmails);
  //     const uxrAndpcounsel = formatPcounselNames([getNameFromLdap(requestor)].concat(pcounselNamesAsArr))
  //     const pcounselNames = formatPcounselNames(pcounselNamesAsArr) //formatPcounselNames(pcounselNamesAsArr)

  //     // replaces static placeholder values withing the html chunk
  //     const simplePlaceHolders = { "\\[UXR FIRST NAME\\], \\[PCounsel name\\]": uxrAndpcounsel, "\\[PCounsel name\\]": pcounselNames, "\\[UXR FIRST NAME\\]": getNameFromLdap(requestor), "\\[Study Name\\]": studyName, "\\[Study name\\]": studyName, "\\[PRODUCT NAME\\]": productName, "\\[TLDR\\]": tldr, "\\[MODERATOR TYPE\\]": moderatorType, "\\[PDD\\]": pdd, "\\[DATE\\]": getDays(7, new Date()) }
  //     const placeHolderUpdater = replacePlaceHolder(simplePlaceHolders)
  //     const textToFnConverter = compose(placeHolderUpdater, getChunkByPlaceHolder, (str) => `[${str}]`, (prop) => prop.hasOwnProperty("cell") ? prop.cell : prop)
  //     const pcounselActionDetailsAsHtml = Array.isArray(pcounselAction) ? pcounselAction.map(textToFnConverter).join("<br/>") : pcounselAction + "<br/>"

  //     const participantTypesAsHtml = Array.isArray(participantTypes) ? participantTypes.join(", ") : participantTypes

  //     const dataTypesAsHtml = Array.isArray(selectedDataTypes) ? selectedDataTypes.map(str => getKeyDataMap(str, "Email")).join(", ") : getKeyDataMap(selectedDataTypes, "Email")
  //     const countriesRequestedAsHtml = Array.isArray(countriesRequested) ? countriesRequested.join(", ") : countriesRequested
  //     // depricated as of 11.06
  //     // const uxrNextStepsAsHtml = Array.isArray(uxrNextSteps) ? uxrNextSteps.map(addParticipantHeader) : addParticipantHeader(uxrNextSteps)

  //     // 11.05 - updates to uxrNextSteps
  //     let uxrNextStepsComponents = uxrNextSteps.map(fetchUxrNextStepsParam);
  //     debugger
  //     let uxrNextStepsAsHtml = uxrNextStepsComponents.reduce(consolidateDuplicateHtmlChunck, [])
  //       .map(convertUxrNextStepsToHtml);
  //     debugger
  //     const notesAsNextStepsAsHtml = Array.isArray(notesNextSteps) ? notesNextSteps.map(textToFnConverter) : textToFnConverter(notesNextSteps)

  //     // replace dynamic placeholders values withing the html chunk
  //     // specifically thos that are derived
  //     const displayLinkTable = getDisplayLinks()
  //     const complexPlaceHolder = { " \\[PCOUNSEL ACTION DETAIL\\]": pcounselActionDetailsAsHtml, "\\[STUDY PARTICIPANT TYPE\\]": participantTypesAsHtml, " \\[TYPE OF PARTICIPANT DATA COLLECTED\\]": dataTypesAsHtml, "\\[COUNTRIES REQUESTED\\]": countriesRequestedAsHtml, "\\[RESEARCHER NEXT STEPS\\]": uxrNextStepsAsHtml.join("<!--joining uxrNextSteps-->"), "\\[IMPORTANT NOTES\\]": notesAsNextStepsAsHtml.join("<!--joining notesAsNextSteps-->"), "\\[HOW TO DISPLAY LINKS\\]": displayLinkTable }

  //     // combine simple and complex place holders to be looped over and populated
  //     const theReplaceables = Object.assign(complexPlaceHolder, simplePlaceHolders)
  //     // loop over key which is search for in htmldoc to be replace by values
  //     for (let placeHolderKey in theReplaceables) {
  //       let placeHolderValue = theReplaceables[placeHolderKey]
  //       let isFound = body.indexOf(placeHolderKey)
  //       let regex = new RegExp(placeHolderKey, "gi")
  //       let match = regex.exec(body)
  //       body = body.replace(regex, placeHolderValue)
  //     }

  //     return body

  //     /* OWNER: douglascox, uxi-automation
  //     * SOURCES: UX Informed Consent
  //     *
  //     * DATE: 2020.08.25
  //     * DESCRIPTION: generates HTML chunk for countries
  //     * @param {(undefined)} - nothing passed ing
  //     * @return {string} - returns the countries display linnks
  //     */
  //     function getDisplayLinks() {
  //       const regionMap = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Region Map")
  //       const { body: eSignatureCountryList } = getDataBySheet(regionMap)

  //       const hasCountryESignature = (country) => {
  //         let countryMatch = eSignatureCountryList.find(props => country === props["Country"].trim()) || [{ eSignature: "No" }]
  //         return countryMatch.hasOwnProperty("eSignatures") ? countryMatch["eSignatures"] === "Yes" ? true : false : false
  //       }
  //       const linkTable = getChunkByPlaceHolder("[linkTable]")
  //       const tableEnd = getChunkByPlaceHolder("[TABLE END]");
  //       const isEnterprise = emailMapR.Enterprise

  //       // combineSimliar country+language powerform links
  //       const powerformLinkReducer = powerFormsUrlsMatch.reduce(countryPowerFormLinkReducer, [])

  //       const powerFormLinks = powerformLinkReducer.map(generatePowerFormTable).join("")
  //       // end of map join html tables

  //       return linkTable + powerFormLinks + tableEnd;

  //       function generatePowerFormTable(props) {
  //         let { "Country+Language": countriesLangues,
  //           "Participant": participant,
  //           "Consumer Alt Link": consumerAltLink,
  //           eSignature: hasEsignature,
  //           "Consumer Instructions 1": consumerInstructions1,
  //           "Consumer Instructions 2": consumerInstructions2,
  //           "Consumer PDF 1": consumerPdf1,
  //           "Consumer PDF 2": consumerPdf2, } = props

  //         let links = isEnterprise ? getEnterpriseLinks() : getConsumerLinks();
  //         let hasPropsConditional = (props, key) => props.hasOwnProperty(key) ? hasHttp(props[key]) ? props[key] : false : false;


  //         let getRepeatingRowByType = repeatingRowType(links)

  //         // determines if powerform links contains data for Minors "ONLY USA" currently has other go to regular repeatingRow
  //         let hasConsumerInstruction2 = consumerAltLink !== "";
  //         /*
  //          hasConsumerInstruction1 && 
  //          !hasAltLink && 
  //          !hasPDF2 && 
  //          !hasConsumerInstruction2
  //         */

  //         const hasMinorParticpants = (participant === "Minors" || participant.includes("Minors"))
  //         const hasHttp = (str) => /http/gi.test(str)
  //         /*
  //           cross reference value in cols for minor to determine if it should be [repeatingRowMinors]  || [repeatingRow]
  //         */
  //         const isCustomFormNeeded = /Custom form needed/gi.test(links.powerFormLink)
  //         const hasConsumerInstructions1 = links.instructions1Link !== ""
  //         const hasConsumerInstructions2 = links.instructions2Link !== ""
  //         const hasAltLink = hasHttp(links.altLink);
  //         const hasPDF2 = hasHttp(links.pdfLink2);
  //         debugger
  //         // let isAltPDF = !links.pdfLink2.includes("http");
  //         /*

  //  return {
  //             powerFormLink: props["Consumer Powerform(s)"],
  //             docLink: hasHttpLink(props["Consumer Doc"]),
  //             pdfLink: hasHttpLink(props["Consumer PDF 1"]),
  //             pdfLink2: hasHttpLink(props["Consumer PDF 2"]),
  //             instructions1Link: props["Consumer Instructions 2"] ,
  //             altLink: hasHttpLink(props["Consumer Alt Link"]),
  //             instructions2Link: props["Consumer Instructions 2"] ,
  //           }
  //         */
  //         const htmlRepeatingRowType = grabHtmlTableType()
  //         Logger.log(`country: ${countriesLangues} htmlTable ${htmlRepeatingRowType}`)

  //         const repeatingRowAsString = getRepeatingRowByType(htmlRepeatingRowType)
  //         /* logic to determine repeating row Type
  //         minor vs eSignature vs default
  //         if minor and eSignature
  //         assumption is eSignature take presidence as its not likely to have minor is no eSig
  //         */
  //         // Logger.log(repeatingRowAsString)
  //         return repeatingRowAsString
  //         /*
  //         repeating row validation: isCustomFormNeeded > validMinorParticipant > hasEsignature > general
  //         */

  //         function grabHtmlTableType() {
  //           const htmlTableRepeatingRowType = {
  //             isCustom: "[repeatingRowCustom]",
  //             hasNoEsig: "[repeatingRowNoESig]",
  //             isInstructions: "[repeatingRowInstructions]",
  //             isInstructionsAlt: "[repeatingRowInstructionsAlt]",
  //             repeatingRow: "[repeatingRow]",
  //             repeatingRowAlt: "[repeatingRowAlt]",
  //           }
  //           const isRepeatingRow = !hasConsumerInstructions1 && !hasAltLink && !hasPDF2 && !hasConsumerInstruction2;
  //           const isRepeatingRowAlt = hasConsumerInstructions1 && !hasAltLink && !hasPDF2 && !hasConsumerInstruction2;
  //           const isRepeatingRowInstructions = hasConsumerInstructions1 && hasAltLink && hasPDF2 && hasConsumerInstruction2;
  //           const isRepeatingRowInstructionsAlt = hasConsumerInstructions1 && hasAltLink && !hasPDF2 && hasConsumerInstruction2

  //           if (isCustomFormNeeded) { return htmlTableRepeatingRowType.isCustom }
  //           if (!hasEsignature) { return htmlTableRepeatingRowType.hasNoEsig }
  //           if (isRepeatingRow) { return htmlTableRepeatingRowType.repeatingRow }
  //           if (isRepeatingRowAlt) { return htmlTableRepeatingRowType.repeatingRowAlt }
  //           if (isRepeatingRowInstructions) { return htmlTableRepeatingRowType.isInstructions }
  //           if (isRepeatingRowInstructionsAlt) { return htmlTableRepeatingRowType.isInstructionsAlt }

  //           return "[repeatingRow]"
  //         }

  //         function hasHttpLink(str) {
  //           return str.indexOf("http") !== -1 ? str : str === "" ? "#" : str
  //         }
  //         function getEnterpriseLinks() {
  //           return {
  //             powerFormLink: props["Enterprise Powerform"],
  //             docLink: hasHttpLink(props["Enterprise Doc"]),
  //             pdfLink: hasHttpLink(props["Enterprise PDF"]),
  //             pdfLink2: hasHttpLink(props["Consumer PDF 2"]),
  //             // no minors on enterprise
  //             instructions1Link: props["Consumer Instructions 1"],
  //             altLink: hasHttpLink(props["Consumer Alt Link"]),
  //             instructions2Link: props["Consumer Instructions 2"],
  //           }
  //         }
  //         function getConsumerLinks() {
  //           return {
  //             powerFormLink: props["Consumer Powerform(s)"],
  //             docLink: hasHttpLink(props["Consumer Doc"]),
  //             pdfLink: hasHttpLink(props["Consumer PDF 1"]),
  //             pdfLink2: hasHttpLink(props["Consumer PDF 2"]),
  //             instructions1Link: props["Consumer Instructions 1"],
  //             altLink: hasHttpLink(props["Consumer Alt Link"]),
  //             instructions2Link: props["Consumer Instructions 2"],
  //           }
  //         }
  //         function repeatingRowType({ powerFormLink, docLink, pdfLink, instructions1Link, altLink, instructions2Link, pdfLink2 }) {
  //           /missing/gi.test(pdfLink) && Logger.log("has 'Missing' as an a href linkLink")
  //           Logger.log(`${JSON.stringify(countriesLangues)}: \n
  //           powerFormLink: ${powerFormLink}, 
  //           docLink: ${docLink},
  //           pdfLink: ${ pdfLink},
  //           instructions1Link: ${instructions1Link},
  //           altLink: ${altLink},
  //           instructions2Link: ${instructions2Link},
  //           pdfLink2: ${pdfLink2}`)

  //           return function (type) {

  //             let html = getChunkByPlaceHolder(type)
  //               .replace(/\[PARTICIPANT TYPE\]/g, participant.join(", "))
  //               .replace(/\[COUNTRY\]/g, countriesLangues.join(", "))
  //               .replace(/\[LINK TO FORM\]/g, powerFormLink)
  //               .replace(/\[LINK TO PDF\]/g, pdfLink)
  //               .replace(/\[LINK TO ALT FORM\]/g, altLink)
  //               .replace(/\[INSTRUCTION PARENT\]/g, instructions1Link)
  //               .replace(/\[INSTRUCTION MINOR\]/g, instructions2Link)
  //               .replace(/\[LINK TO PDF2\]/g, pdfLink2)

  //             return html
  //           }
  //         }
  //       }
  //       function countryPowerFormLinkReducer(acc, next) {
  //         let isEnterprise = emailMapR.Enterprise

  //         let isCountryEsignature = hasCountryESignature(next["Country+Language"])

  //         let { "Country+Language": countryLanguage,
  //           "Participant": participant,
  //           "Data": dataType,
  //           "Consumer Powerform(s)": consumerPowerLink,      //isUsed
  //           "Consumer PDF 1": consumerPdf1,                   //isUsed
  //           "Consumer Alt Link": consumerAltLink,             //isUsed
  //           "Consumer PDF 2": consumerPdf2,                 //isUsed
  //           "Consumer Doc": consumerDoc,                     //isUsed
  //           "Enterprise Powerform": enterprisePowerForm,     //isUsed
  //           "Enterprise Doc": enterpriseLink,                //isUsed
  //           "Enterprise PDF": enterprisePdf,                  //isUsed,
  //         } = next
  //         next["eSignature"] = hasCountryESignature(next["Country+Language"])

  //         let isParticipantTypeMatch = (props) => props["Participant"].includes(participant);
  //         let isLinkParticipantMatch = (props) => {
  //           let {
  //             "Consumer Powerform(s)": currConsumerPowerLink,      //isUsed
  //             "Consumer PDF 1": currConsumerPdf1,                   //isUsed
  //             "Consumer Alt Link": currConsumerAltLink,             //isUsed
  //             "Consumer PDF 2": currConsumerPdf2,                 //isUsed
  //             "Consumer Doc": currConsumerDoc,                     //isUsed
  //             "Enterprise Powerform": currEnterprisePowerForm,     //isUsed
  //             "Enterprise Doc": currEnterpriseLink,                //isUsed
  //             "Enterprise PDF": currEnterprisePdf                  //isUsed
  //           } = props;
  //           // // const hasCustom = (arr) => arr.every( str => /custom/gi.test(str))
  //           // // const hasCustomPowerForm =  emailMapR.Enterprise ? hasCustom(currEnterprisePowerForm, enterprisePowerForm) : hasCustom(currConsumerPowerLink, consumerPowerLink) 
  //           // const matchTypes = emailMapR.Enterprise
  //           //   ? [
  //           //     [enterprisePowerForm, currEnterprisePowerForm],
  //           //     [enterpriseLink, currEnterpriseLink],
  //           //     [enterprisePdf, currEnterprisePdf]]
  //           //   : [[consumerPowerLink, currConsumerPowerLink],
  //           //   [consumerPdf1, currConsumerPdf1],
  //           //   [consumerAltLink, currConsumerAltLink],
  //           //   [consumerPdf2, currConsumerPdf2],
  //           //   [consumerDoc, currConsumerDoc],
  //           //   ]
  //           // isCustomPDF == custom ignore match on consumerDoc
  //           // hi
  //           const hasCustom = (arr) => arr.every(str => /custom/gi.test(str))
  //           const hasCustomPowerForm = emailMapR.Enterprise ? hasCustom([currEnterprisePowerForm, enterprisePowerForm]) : hasCustom([currConsumerPowerLink, consumerPowerLink])
  //           const consumerMatch = [
  //             [consumerPowerLink, currConsumerPowerLink],
  //             [consumerPdf1, currConsumerPdf1],
  //             [consumerAltLink, currConsumerAltLink],
  //             [consumerPdf2, currConsumerPdf2],
  //             //          [consumerDoc, currConsumerDoc],
  //           ]
  //           const enterpriseMatch = [
  //             [enterprisePowerForm, currEnterprisePowerForm],
  //             //  [enterpriseLink, currEnterpriseLink],
  //             [enterprisePdf, currEnterprisePdf]]
  //           !hasCustomPowerForm && enterpriseMatch.push([enterpriseLink, currEnterpriseLink]) && consumerMatch.push([consumerDoc, currConsumerDoc])
  //           const matchTypes = emailMapR.Enterprise ? enterpriseMatch : consumerMatch
  //           // by


  //           let isPowerFormMatch = matchTypes.every((arr) => isSame(arr[0], arr[1]))
  //           let findMatchTypes = matchTypes.map((arr) => isSame(arr[0], arr[1]))


  //           return isPowerFormMatch
  //           function isSame(str1, str2) { return str1 === str2 }
  //         }
  //         let isEsigMatch = (props) => { ; return props["eSignature"] === next["eSignature"] }

  //         let accMatch = acc.find(props => isLinkParticipantMatch(props) && isParticipantTypeMatch(props) && isEsigMatch(props)) || false
  //         if (accMatch) {
  //           !accMatch["Country+Language"].includes(countryLanguage) && accMatch["Country+Language"].push(countryLanguage)
  //           !accMatch["Participant"].includes(participant) && accMatch["Participant"].push(participant)
  //         } else if (acc.length !== 0) {

  //         }

  //         // let isLinksMatch = isParticipantTypeMatch && isLinkParticipantMatch
  //         let copyObj = Object.assign({}, next)
  //         let objWithCountryParticipantAsArr = Object.assign(copyObj, { "Country+Language": [countryLanguage], "Participant": [participant] })

  //         return !!accMatch ? acc : [...acc, objWithCountryParticipantAsArr]
  //       }
  //     }

  //     function consolidateDuplicateHtmlChunck(acc, next, idx, self) {
  //       let current = acc.find(props => props.placeHolder === next.placeHolder)
  //       let hasPlaceHolder = !!current

  //       hasPlaceHolder && addToHeaderToExisting(current);

  //       return hasPlaceHolder ? acc : [...acc, next]

  //       function addToHeaderToExisting(current) {
  //         const hasProperty = (prop) => current[prop].includes(next[prop])
  //         // const hasHeaderAlready = current.uxrNextStepsHeader.includes(next.uxrNextStepsHeader)
  //         const hasHeaderAlready = hasProperty("uxrNextStepsHeader")
  //         const hasParticipantType = hasProperty("participantType")
  //         if (!hasHeaderAlready) {
  //           current.uxrNextStepsHeader += `<br> ${next.uxrNextStepsHeader}`
  //         }
  //         if (!hasParticipantType && !hasHeaderAlready) {
  //           current.participantType += `, ${next.participantType}`
  //         }
  //       }
  //     }

  //     function convertUxrNextStepsToHtml({ placeHolder, htmlChunk, uxrNextStepsHeader, participantType }) {

  //       return htmlChunk.replace(/\[ParticipantHeader]/g, uxrNextStepsHeader)
  //     }

  //     function fetchUxrNextStepsParam(nextSteps) {
  //       let placeHolder = "[" + nextSteps.cell + "]"
  //       let htmlChunk = getChunkByPlaceHolder(placeHolder)
  //       let headerPlacementObj = headerNextSteps.find(fetchParticipantHeader);
  //       let participantType = nextSteps.participantTypeFromMatrix

  //       return {
  //         placeHolder,
  //         htmlChunk,
  //         uxrNextStepsHeader: headerPlacementObj.hasOwnProperty('cell') ? headerPlacementObj.cell : " ",
  //         participantType
  //       }

  //       function fetchParticipantHeader(props) {
  //         const isObjectMatch = (obj1, obj2) => (key) => obj1[key] === obj2[key]
  //         const checkObjMatch = isObjectMatch(nextSteps, props)
  //         let isSurveyMatch = checkObjMatch("isSurveyFromMatrix")
  //         let isRegionMatch = checkObjMatch("regionFromMatrix")
  //         let isParticipantTypeMatch = checkObjMatch("participantTypeFromMatrix")
  //         // this conditional was asked to be removed
  //         // let isDataTypateMatch = checkObjMatch("dataTypeFromMatrix")
  //         return isSurveyMatch && isRegionMatch && isParticipantTypeMatch //&& isDataTypateMatch
  //       }
  //     }

  //     // replaced by fetchParticipantHeader
  //     function addParticipantHeader(nextSteps) {
  //       let placeHolder = "[" + nextSteps.cell + "]"
  //       let htmlChunk = getChunkByPlaceHolder(placeHolder)

  //       let participantHeader = headerNextSteps.find(props => {
  //         const isObjectMatch = (obj1, obj2) => (key) => obj1[key] === obj2[key]
  //         const checkObjMatch = isObjectMatch(nextSteps, props)
  //         let isSurveyMatch = checkObjMatch("isSurveyFromMatrix")
  //         let isRegionMatch = checkObjMatch("regionFromMatrix")
  //         let isParticipantTypeMatch = checkObjMatch("participantTypeFromMatrix")
  //         // let isDataTypateMatch = checkObjMatch("dataTypeFromMatrix")
  //         return isSurveyMatch && isRegionMatch && isParticipantTypeMatch// && isDataTypateMatch
  //         /* 11.04 updates
  //     const isObjectMatch = (obj1, obj2) => (key) => obj1[key] === obj2[key];
  //     const checkObjMatch = isObjectMatch(nextSteps, props)
  //     const inclusiveUxrNextStepsMatch = (obj1, obj2) => (type) => (new RegExp(obj2[type]).test(obj1[type]) ) 
  //     const inclusiveMatch = inclusiveUxrNextStepsMatch(nextSteps, props)
  //     let isRegionMatch = inclusiveMatch("regionFromMatrix")
  //     let isSurveyMatch = checkObjMatch("isSurveyFromMatrix")
  //     let isParticipantTypeMatch = inclusiveMatch("participantTypeFromMatrix")
  //     return isSurveyMatch && isRegionMatch && isParticipantTypeMatch
  //     */
  //       }) || { cell: placeHolder + " Unable to match with sheet 'headerMatrix' for a predetermind response." }
  //       let isFound = new RegExp(placeHolder, "gi").test(htmlChunk)
  //       /* 11.04 update
  //       let isSameUXRNextSteps = !!selfArray.reduce( (acc, next) => acc.cell === next.cell ? next : false) ;
  //       let participantHeaderText = isSameUXRNextSteps ? "" : participantHeader
  //       return htmlChunk.replace(/\[ParticipantHeader]/g, participantHeaderText)
  //       */
  //       return htmlChunk.replace(/\[ParticipantHeader]/g, participantHeader.cell)
  //     }
  //   }

  //   function replacePlaceHolder(props) {
  //     return function (str) {
  //       // Logger.log(str)
  //       const templateHolders = Object.keys(props)
  //       templateHolders.forEach(template => {
  //         regextemplate = template.replace("[", "\\[")
  //           .replace("]", "\\]")
  //         let regex = new RegExp(regextemplate, 'i')
  //         let boo = regex.test(str)
  //         let replaceWith = props[template]
  //         str = str.replace(regex, props[template])

  //       })
  //       return str
  //     }
  //   }



  //   /*
  //     repalce holders varaible with 
  //   */

  //   function getNameFromLdap(ldap) {
  //     if (ldap.includes(',')) {
  //       return ldap.split(',').map(str => getNameFromLdap(str.trim())).join(', ')
  //     }
  //     const isGoogler = ldap.includes('@google')
  //     const atSymbolIdx = ldap.indexOf('@google')
  //     const person = getGooglerInfo(ldap.substring(0, atSymbolIdx))
  //     const personName = person.hasOwnProperty('firstName') ? person.firstName : person.concat('@google.com')
  //     return isGoogler ? personName : ldap
  //   }
  //   function getHtmlChunk(fullDocText) {
  //     return function (str) {
  //       const strIdx = fullDocText.indexOf(str)
  //       const body = strIdx !== -1 ? fullDocText.substring(strIdx + str.length, fullDocText.indexOf("[END]", fullDocText.indexOf(str))) : "[NOT FOUND] " + str + "  in fullDocText<br><br>";
  //       // const notFound = strIdx === -1 ? "[NOT FOUND] "+ str + "  in fullDocText"
  //       return body
  //     }
  //   }
  // }

  function printSubject(subject, activeRow) {
    var s = SpreadsheetApp.getActive();
    var sheet = s.getSheetByName("Form Responses 2");
    var values = sheet.getDataRange().getValues();
    var subjectCOL = values[0].indexOf('Email Subject Line') + 1;
    try {
      sheet.getRange(activeRow, subjectCOL, 1, 1).setValue(subject);
    }
    catch (err) {
      Logger.log(catchToString(err));
    }
  }

  function getDays(numDays, startDate) {
    var tempDay = startDate;

    tempDay.setDate(tempDay.getDate() + numDays)

    var formattedDate = Utilities.formatDate(tempDay, "PST", "MMMM dd, YYYY");
    return formattedDate;

  }


  function testGetGooglerInfo() {
    var person = getGooglerInfo("jgolenbock@google.com");

    Logger.log(person);
  }

  // Get person info using AdminDirectory.
  // All users are assumed to be registered in ldap as userName@google.com
  function getGooglerInfo(userName) {
    if (userName.includes('@google'))
      userName = typeof userName === "string" ?
        userName = userName.slice(0, userName.indexOf('@') === -1 ? userName.length : userName.indexOf('@'))
        : userName
    var person = null;
    // if NOT AdminDirectory Exist then

    if (typeof AdminDirectory !== 'object') {
      return `[placeHolder name for ${userName}]`
    }

    try {
      var resp = AdminDirectory.Users.get(userName + "@google.com", { 'viewType': 'domain_public', 'projection': 'full' });
      if (resp != null) {
        var p = resp;
        person = {};
        // Safe mapping to bring data if it can be found under the deep nested hierarchy.


        try {
          person.firstName = p.name.givenName;
          // person.lastName = p.name.familyName; 
          person.fullName = p.name.fullName;
        } catch (e) {
          //Logger.log("Name not found for %s", userName); 
        };


      }
      return person;
    }
    catch (e) {
      var exp = e;
      Logger.log(e);

      //returns LDAP if real name isn't found in teams
      Logger.log("%s not found on` teams", userName);
      //return userName.concat('@google.com')

    }
    return userName;
  }

  function checkForBounceBacks() {
    var s = SpreadsheetApp.getActive();
    var sheet = s.getSheetByName("Form Responses 2");
    var values = sheet.getDataRange().getValues();
    var emailAddresCOL = values[0].indexOf('Email Address') + 1;
    var bouncebackStatusCOL = values[0].indexOf('Bounceback Status') + 1; //values[i][13]
    var bouncebackEmailAddressCOL = values[0].indexOf('Bounceback Email Address') + 1; //values[i][14]

    const pcounselCOL = values[0].indexOf("Who is your PCounsel? Enter the full email address (ldap@google.com). ") + 1;
    const ccAddressCOL = values[0].indexOf("Who else should be cc'd on the email? (ldap@google.com, ldap@google.com)")

    var conversations2 = GmailApp.search("\"Your message wasn't delivered to \"" + " newer_than:3d");

    var thread;

    var string1 = "Your message wasn't delivered to <a style='color:#212121;text-decoration:none'><b>";
    var string2 = "</b></a> because the address couldn't be found, or is unable to receive mail."

    var myDoc = DocumentApp.openById("1QBVhWKtlA95luujmz77_XHH5OSpppZ-KsbfPsK3HxZ4");
    var fullDoc = myDoc.getBody();
    var fullDocText = fullDoc.getText();
    var bouncebackHtmlBeginningtag = '[BODY Response to bounceback email]'
    var tableEnd = fullDocText.substring(fullDocText.indexOf(bouncebackHtmlBeginningtag) + (bouncebackHtmlBeginningtag.length), fullDocText.indexOf("[END]", fullDocText.indexOf(bouncebackHtmlBeginningtag)));
    debugger
    for (var c = 0; c < conversations2.length; c++) {
      thread = conversations2[c];
      debugger
      for (var k = 1; k <= thread.getMessageCount(); k++) { //might send 2+ emails with bounceback
        if (thread.getMessageCount() > 1) {
          try {
            Logger.log('getFrom: ' + thread.getMessages()[k].getFrom());
            if (thread.getMessages()[k].getFrom() == "Mail Delivery Subsystem <mailer-daemon@googlemail.com>") {
              var body = thread.getMessages()[k].getBody();
              var errorEmail = body.substring(body.indexOf(string1) + string1.length, body.indexOf(string2));
              Logger.log('errorEmail: ' + errorEmail);
              var errorDate = thread.getMessages()[1].getDate();

              //blank, first check complete, verified, bounceback
              for (var i = values.length - 1; i > values.length - 50; i--) {
                var pcounselValue = values[i][pcounselCOL];
                var ccAddressValue = values[i][ccAddressCOL]
                var bouncebackStatus = sheet.getRange(i + 1, bouncebackStatusCOL, 1, 1).getValue();
                var emailAddress = sheet.getRange(i + 1, emailAddresCOL, 1, 1).getValue(); //requestor email
                //Logger.log('bouncebackStatus: ' + bouncebackStatus + ' row: ' + (i+1));
                Logger.log('column 8 & 9 row: ' + i + ' ' + values[i][7] + ', ' + values[i][8]);

                if ((pcounselValue.indexOf(errorEmail) >= 0) || (ccAddressValue.indexOf(errorEmail) >= 0)) {
                  //thread.getMessages()[1].forward("jgolenbock@google.com");
                  //if (bouncebackStatus == '' || bouncebackStatus == 'Bounceback'){
                  var myDate = new Date(errorDate);
                  var myTime = myDate.getTime();
                  var currentDate = new Date();
                  var currentTime = currentDate.getTime();
                  Logger.log('myTime: ' + myTime);
                  Logger.log('currentTime: ' + currentTime);
                  Logger.log('Row of email: ' + i + ' ' + errorEmail);
                  var numberOfMinutesAgo = Math.round((currentTime - myTime) / 60000)
                  Logger.log("The email was sent " + numberOfMinutesAgo + " minutes ago");

                  if (bouncebackStatus == '') {
                    //sheet.getRange(i+1, colN, 1, 1).setValue('Bounceback');
                    //sheet.getRange(i+1, colM, 1, 1).setValue(errorEmail);
                    if (pcounselValue.indexOf(errorEmail) >= 0) { //PC
                      var pCounsel = true;
                      emailBounceBacks(pCounsel, emailAddress, errorEmail);
                      sheet.getRange(i + 1, bouncebackStatusCOL, 1, 1).setValue('Bounceback');
                      sheet.getRange(i + 1, bouncebackEmailAddressCOL, 1, 1).setValue(errorEmail);
                      //break;
                    }
                    else if (ccAddressValue.indexOf(errorEmail) >= 0) { //Other
                      var pCounsel = false;
                      emailBounceBacks(pCounsel, emailAddress, errorEmail);
                      sheet.getRange(i + 1, bouncebackStatusCOL, 1, 1).setValue('Bounceback');
                      sheet.getRange(i + 1, bouncebackEmailAddressCOL, 1, 1).setValue(errorEmail);
                      //break;
                    }
                    //return;
                  }
                  else if (bouncebackStatus == 'Bounceback' && values[i][bouncebackStatusCOL].indexOf(errorEmail) < 0) {
                    var bouncebackEmail = sheet.getRange(i + 1, bouncebackEmailAddressCOL, 1, 1).getValue();
                    Logger.log('values[i][15].indexOf(errorEmail): ' + values[i][15].indexOf(errorEmail));
                    //sheet.getRange(i+1, colM, 1, 1).setValue(bouncebackEmail + ', ' + errorEmail);

                    if (pcounselValue.indexOf(errorEmail) >= 0) { //PC
                      var pCounsel = true;
                      sheet.getRange(i + 1, bouncebackEmailAddressCOL, 1, 1).setValue(bouncebackEmail + ', ' + errorEmail);
                      emailBounceBacks(pCounsel, emailAddress, errorEmail);
                    }
                    else if (ccAddressValue.indexOf(errorEmail) >= 0) { //Other
                      var pCounsel = false;
                      sheet.getRange(i + 1, bouncebackEmailAddressCOL, 1, 1).setValue(bouncebackEmail + ', ' + errorEmail);
                      emailBounceBacks(pCounsel, emailAddress, errorEmail);
                    }
                    return;
                  }
                }
              }
            }
          }
          catch (err) {
            Logger.log(catchToString(err)); //alentz
          }
        }
      }
    }
  }

  function emailBounceBacks(pCounsel, emailAddress, errorEmail) {
    var myDoc = DocumentApp.openById("1QBVhWKtlA95luujmz77_XHH5OSpppZ-KsbfPsK3HxZ4");
    var fullDoc = myDoc.getBody();
    var fullDocText = fullDoc.getText();
    var bouncebackHtmlBeginningtag = '[BODY Response to bounceback email]';
    //var pCounsel = true;
    //var testFirstName = 'Mike';
    var requestorName = getGooglerInfo(emailAddress).firstName;
    //var testBadEmail = 'bonaroTest@google.com'

    if (pCounsel == true) {
      var emailText = fullDocText.substring(fullDocText.indexOf(bouncebackHtmlBeginningtag) + (bouncebackHtmlBeginningtag.length), fullDocText.indexOf("[ENDBB1]", fullDocText.indexOf(bouncebackHtmlBeginningtag)));
      emailText = emailText + fullDocText.substring(fullDocText.indexOf("[ENDBB2]") + 8, fullDocText.indexOf("[ENDBB3]", fullDocText.indexOf(bouncebackHtmlBeginningtag)));
      emailText = emailText.replace("[IF PCOUNSEL]", '')
        .replace("[UXR FIRST NAME]", requestorName)
        .replace("[EMAIL THAT GOT BOUNCEBACK]", errorEmail);
    }
    else if (pCounsel == false) {
      var emailText = fullDocText.substring(fullDocText.indexOf(bouncebackHtmlBeginningtag) + (bouncebackHtmlBeginningtag.length), fullDocText.indexOf("[IF PCOUNSEL]", fullDocText.indexOf(bouncebackHtmlBeginningtag)));
      emailText = emailText + fullDocText.substring(fullDocText.indexOf("[IF OTHER]") + 10, fullDocText.indexOf("[ENDBB3]", fullDocText.indexOf(bouncebackHtmlBeginningtag)));
      emailText = emailText.replace("[ENDBB2]", '')
        .replace("[UXR FIRST NAME]", requestorName)
        .replace("[EMAIL THAT GOT BOUNCEBACK]", errorEmail);
    }

    var subject = 'Oops - there’s a problem with your UX Informed Consent request';
    var message = emailText;
    //var recipient = 'bonaro@google.com'; //testing
    var recipient = emailAddress; //Live
    var ccAddress = '';

    emailToRecipient(subject, message, recipient, ccAddress);
  }



  function checkForOOO() {
    var ss = SpreadsheetApp.getActive();
    var s = ss.getSheetByName("Form Responses 2");
    var values = s.getDataRange().getValues();

    var emailAddresCOL = values[0].indexOf('Email Address') + 1;
    var subjectCOL = values[0].indexOf('Email Subject Line') + 1;
    var oooStatusCOL = values[0].indexOf('OOO Status') + 1;
    var oooEmailAddressCOL = values[0].indexOf('OOO Email Address') + 1;
    var pcounselCol = values[0].indexOf('Who is your PCounsel? Enter the full email address (ldap@google.com). ')
    debugger
    for (var c = s.getLastRow() - 1; c >= 0; c--) { //check each line of the sheet
      Logger.log(`c: ${c}`)
      var emailAddress = s.getRange(c + 1, emailAddresCOL, 1, 1).getValue();
      var subjectLine = s.getRange(c + 1, subjectCOL, 1, 1).getValue();
      var oooStatus = s.getRange(c + 1, oooStatusCOL, 1, 1).getValue();
      var oooEmail = s.getRange(c + 1, oooEmailAddressCOL, 1, 1).getValue();
      Logger.log('subjectLine: ' + subjectLine);
      //Logger.log('values[c][7]: ' + values[c][7]);
      if (subjectLine != '' && oooStatus != 'OOO' && oooStatus != 'verified') {
        var subjectSearch = 'Re: ' + subjectLine;
        Logger.log('subjectSearch: ' + subjectSearch);
        var conversations = GmailApp.search('subject:' + subjectSearch + ' newer_than:3d'); //search based on subject line in the approved row
        //var conversations = GmailApp.search("Re: " + subjectLine + ' newer_than:3d');
        //GmailApp.search("\"There was a problem delivering your message to\""
        //var threads = GmailApp.search('subject:' + subject3 + ' newer_than:3d');
        //Logger.log('"\"Re: \"" + subjectLine: ' + "\"Re: \"" + subjectLine);
        Logger.log('conversations.length: ' + conversations.length);
        Logger.log('conversations: ' + conversations);
        if (conversations.length <= 0) {
          if (oooStatus == '') {
            s.getRange(c + 1, oooStatusCOL, 1, 1).setValue('first check');
          }
          if (oooStatus == 'first check') {
            s.getRange(c + 1, oooStatusCOL, 1, 1).setValue('verified');
          }
        }
        for (var i = 0; i < conversations.length; i++) {
          Logger.log('conversations.length: ' + conversations.length);
          var pcounselValue = values[c][pcounselCol]
          var thread = conversations[i];
          Logger.log('messageCount: ' + thread.getMessageCount())
          if (thread.getMessageCount() > 0) { //might need to make this == 2?
            var messages = thread.getMessages();
            for (var j = 0; j < messages.length; j++) {
              var message = thread.getMessages()[j];
              //Logger.log('substring get from: ' + message.getFrom().substr(message.getFrom().indexOf('<')+1, message.getFrom().indexOf('@google.com')-1));
              //Logger.log('substring get from: ' + message.getFrom().split(/[<>]/));
              //Logger.log('values[c][7]: ' + values[c][7]);
              Logger.log('message.getFrom(): ' + message.getFrom());
              if (message.getFrom().indexOf(pcounselValue) >= 0) { //only need to check if the PC email is ooo
                Logger.log('thread: ' + i + ' message: ' + j + ' from: ' + message.getFrom());
                Logger.log('body: ' + message.getPlainBody().substr(0, message.getPlainBody().indexOf('\n> '))); //need to insert into email to requestor
                Logger.log('body: ' + message.getPlainBody());
                Logger.log('values[c][7]: ' + values[c][7]);
                Logger.log('pcounsel: ' + pcounselValue)
                //var oooText = message.getPlainBody().substr(0, message.getPlainBody().indexOf('\n> '));
                //Logger.log('oooText: ' + oooText);
                //get the first name of the UXR and check the body of the email for it  
                var userInfo = getGooglerInfo(emailAddress.substr(0, emailAddress.indexOf("@google.com")));
                var firstName = userInfo.firstname;
                Logger.log('firstName: ' + firstName);
                if (message.getPlainBody().indexOf(firstName) < 0) { //check if the first name of the UXR is in the body of the email
                  if (message.getPlainBody().indexOf('\n> ') < 0) {
                    var oooText = message.getPlainBody();
                  }
                  else {
                    var oooText = message.getPlainBody().substr(0, message.getPlainBody().indexOf('\n> '));
                  }
                }
                else {
                  return;
                }
                emailOOO(values[c][7], emailAddress, oooText);
                s.getRange(c + 1, oooStatusCOL, 1, 1).setValue('OOO');

                s.getRange(c + 1, oooEmailAddressCOL, 1, 1).setValue(pcounselValue);
                //else {s.getRange(c+1, colQ, 1, 1).setValue('first check');} //if there is a bounceback there is an issue
              }//if close
              else {
                if (oooStatus == '') {
                  s.getRange(c + 1, oooStatusCOL, 1, 1).setValue('first check');
                }
                if (oooStatus == 'first check') {
                  s.getRange(c + 1, oooStatusCOL, 1, 1).setValue('verified');
                }
              }
            }//for[j] close
          }//if close
          else {
            if (oooStatus == '') {
              s.getRange(c + 1, oooStatusCOL, 1, 1).setValue('first check');
            }
            if (oooStatus == 'first check') {
              s.getRange(c + 1, oooStatusCOL, 1, 1).setValue('verified');
            }
          }//else close
        }//for[i] close
      }//if close
    }//for[c] close
  }//fun      


  function emailOOO(errorEmail, emailAddress, oooText) {
    var myDoc = DocumentApp.openById("1QBVhWKtlA95luujmz77_XHH5OSpppZ-KsbfPsK3HxZ4")
    var fullDoc = myDoc.getBody();
    debugger
    var fullDocText = fullDoc.getText();
    var oooHtmlBeginningtag = '[BODY RESPONSE TO PCOUNSEL OOO]';
    var requestorName = getGooglerInfo(emailAddress).firstName;
    //var testBadEmail = 'bonaroTest@google.com'

    var emailText = fullDocText.substring(fullDocText.indexOf(oooHtmlBeginningtag) + (oooHtmlBeginningtag.length), fullDocText.indexOf("[END]", fullDocText.indexOf(oooHtmlBeginningtag)));
    emailText = emailText.replace(/\[UXR FIRST NAME\]/, requestorName)
      .replace(/\[INSERT TEXT\]/, oooText);


    var subject = 'Oops - your PCounsel is OOO';
    var message = emailText;
    //var recipient = 'bonaro@google.com'; //testing
    var recipient = emailAddress; //Live
    var ccAddress = '';

    //  emailToRecipient(subject, message, recipient, ccAddress);
  }



  function emailToRecipient(subject, message, recipient, ccAddress) { //Generic email sending function
    var mailFromIndex = GmailApp.getAliases().indexOf("ux-consent@google.com");
    var mailFrom = GmailApp.getAliases()[mailFromIndex];

    //  GmailApp.sendEmail(recipient, subject, message, { from: mailFrom, cc: ccAddress, replyTo: mailFrom, htmlBody: message });
    //Logger.log(message);
  }


  function catchToString(err) { //Granular error message
    var errInfo = "Done Catched Something:  (╯°□°)╯︵ ┻━┻\n";
    for (var prop in err) {
      errInfo += "  property: " + prop + "\n    value: [" + err[prop] + "]\n";
    }
    errInfo += "  toString(): " + " value: [" + err.toString() + "]";
    return errInfo;
  }

  function joinVals(rangeValues) {
    function notBlank(element) { return element != ''; }
    return rangeValues[0].filter(notBlank);
  }


}
