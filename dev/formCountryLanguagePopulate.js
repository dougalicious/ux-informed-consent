function formCountryLanguagePopulateAnswers() {
  const formUrl = SpreadsheetApp.getActiveSpreadsheet().getFormUrl()
  const formApp = FormApp.openByUrl(formUrl)
  const regionMap = getCountries()
  const allCountries = Object.keys(regionMap).reduce((acc, next) => acc.concat(regionMap[next]), [])
  
  const questions = {
    "APAC countries": regionMap["APAC"],
    "EMEA countries": regionMap["EMEA"],
    "LATAM countries": regionMap["LATAM"],
    "NA countries": regionMap["NA"],
    "In which countries will the participants be located? ": allCountries,
  }
  let apacItem = formApp.getItems().filter(item => item.getTitle().includes("APAC countries"))
  const filterByStr = (str) => formApp.getItems().filter(item => item.getTitle().includes(str))
  let countryItems = Object.keys(questions).map( countryStr => {
    let [item] = filterByStr(countryStr)
    let prop = {}
    item.asCheckboxItem().setChoiceValues( questions[countryStr])
    prop[countryStr] = item.asCheckboxItem()
    return prop
  })
  debugger
}
function getCountries() {
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheet = ss.getSheetByName("Region Map")
  const dataBySheet = getDataBySheet(sheet);
  const { body } = dataBySheet
  return body.reduce((acc, props) => {
    let region = props["Region"]
    let country = props["Country"].trim()
    if (acc.hasOwnProperty(region)) {
      acc[region].push(country)
    } else {
      acc[region] = [country]
    }
    return acc
  }, {})

  debugger
}
