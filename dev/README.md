# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Status ###

* contains updates in reference to [Consent form link missing from notification emails](https://buganizer.corp.google.com/issues/193135001)
* may contains update relative to [[Horizontal] UXIC: Sensitive Demographic Updates](https://buganizer.corp.google.com/issues/191718916)
### What is this repository for? ###

* Staging | Dev version of Live Informed Consent script
* used as working environment for updates and bug fixes
  * please note the development of features coincided with bug fixes. Although the various feature updates are relative to additional demographic to be added, therefore shouldn't impact the code logic. Although i have noticed some of these updates to be on the live version, to there's likely some overlap
* Version
* [bitbucket link](https://bitbucket.org/dougalicious/ux-informed-consent/src/master/)
