/*
 *
 *   typeMatch: typeMatch,
    reasonMatch: () => null,
    storageMatch: () => null
 * 
 */
/*
  compose(question, answers, namedValues) => boolean
*/
function fetchQuestion(question) {
  return function (answers) {
    return function ({ namedValues }) {
      const responseHasQuestion = namedValues.hasOwnProperty(question)
      const response = responseHasQuestion ? namedValues[question][0] : ""
      const hasQuestion = responseHasQuestion ? response !== "" : false;
      const hasValidResponse = answers.some(answer => response.includes(answer))
      return hasQuestion ? hasValidResponse : false
    }
  }
}



function getQandACriteria({ question, answer }) {
  return function (namedValues) {
    let response = namedValues.hasOwnProperty(question) ? namedValues[question][0] : ""
    return response === "" ? false : response.includes(answer)
  }
}

function fetchSensitiveDemographic(e) {
   e = e || artificalEvent();

  /** Question mapping  */
  const whyDataType = fetchQuestion("Why are you collecting sensitive demographic data (ethnicity, race, or sexual orientation)?");
  const whatDataType = fetchQuestion("What type of data are you collecting in the study? (Check all that apply)");
  const whoDataType = fetchQuestion("Who will be managing collection and storage of participant data?");

  const isRaceOrEthnicity = whatDataType(["Race or ethnicity"]);
  const isSexOrientation = whatDataType(["Sexual orientation"]);
  const isEnsureDiverseParticipant = whyDataType(["To ensure a more diverse set of participants"]);
  const hasWhoDataTypeUXIAndG = whoDataType([
    "UX Infrastructure",
    "A research agency (who will share sensitive participant demographic data with Google in an aggregated and de-identified format)"
  ])
  const hasWhoDataTypeMeAndGFull = whoDataType([
    "Me (or another UXR I'm working with)",
    "A research agency (who will share sensitive participant demographic data with Google in full)"
  ]);
  const hasHighWhyResponse = whyDataType(["To draw conclusions or report insights based on this data"]);

  /** demographic categories *specific to ethnicity & orientation */
  const sensitiveDemographic = [
    {ethnicityRace_low: isEthnicityRaceLow},
    {ethnicityRace_med: isEthnicityRaceMed},
    {ethnicityRace_high: isEthnicityRaceHigh},
    {sexOrientation_low: isSexOrientationLow},
    {sexOrientation_med: isSexOrientationMed},
    {sexOrientation_high: isSexOrientationHigh}
  ].reduce( (acc, props) => {
    let [[demoType, fn]] = Object.entries(props)
    return fn(e) ? acc.concat(demoType) : acc
  }, [])
  const addDemographicType = (acc, next) => acc.concat(next)
  let demographicAdditions = sensitiveDemographic.reduce( addDemographicType , [])
  return demographicAdditions 
   
  function isEthnicityRaceLow({ namedValues }) {
    const ethnicityRaceLowType = [
      isRaceOrEthnicity,
      isEnsureDiverseParticipant,
      hasWhoDataTypeUXIAndG
    ];
    let isMatch = ethnicityRaceLowType.every(fn => fn({ namedValues }))
    return isMatch
  }
  function isEthnicityRaceMed({ namedValues }) {
    const ethnicityRaceMedType = [
      isRaceOrEthnicity,
      isEnsureDiverseParticipant,
      hasWhoDataTypeMeAndGFull
    ];
    let isMatch = ethnicityRaceMedType.every(fn => fn({ namedValues }))
    return isMatch
  };
  function isEthnicityRaceHigh({ namedValues }) {
    const ethnicityRaceHighType = [
      isRaceOrEthnicity,
      hasHighWhyResponse,
    ];
    let isMatch = ethnicityRaceHighType.every(fn => fn({ namedValues }))
    return isMatch
  }
  function isSexOrientationLow({ namedValues }) {
    const sexOrientationLow = [
      isSexOrientation,
      isEnsureDiverseParticipant,
      hasWhoDataTypeUXIAndG
    ]
    let isMatch = sexOrientationLow.every(fn => fn({ namedValues }));
    const a = isMatch
    return isMatch
  }
  function isSexOrientationMed({ namedValues }) {
    const validationChecks = [
      isSexOrientation,
      isEnsureDiverseParticipant,
      hasWhoDataTypeMeAndGFull
    ]
    let isMatch = validationChecks.every(fn => fn({ namedValues }))
    return isMatch
  }
  function isSexOrientationHigh({ namedValues }) {
    const validationChecks = [
      isSexOrientation,
      hasHighWhyResponse
    ]
    let isMatch = validationChecks.every(fn => fn({ namedValues }))
    return isMatch
  }

 
}


