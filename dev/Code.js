// cms location https://docs.google.com/spreadsheets/d/1SK_cutW4H5tC2ZgWbAiLLqHpZeNAuA5B0LiAC3CrhjQ/edit
// webapp location https://script.google.com/a/google.com/d/1RKiK1S5OW82yggeRXneDben23B-6iGINVXCtku6PbvGDjw-NuvoQQNse/edit?usp=drive_web
// form: https://script.google.com/a/google.com/macros/s/AKfycbyuHKdxf-p0mkWr87ixMyDeGyUEf-QfD1phzbcwiYtTeWIV_Vc/exec
// trix: https://docs.google.com/spreadsheets/d/1MoOeqnmNxOKjMSIDS_buteMtKwCvxR3CmG1vM6DsDOA/edit?resourcekey=0-0jqI7khLwTgEUaVKHJNXaQ#gid=2002425214

function passSheetDataToOnFormSubmit(){
  const list = grabListOfResponsesToEmail()
  const process = compose(__onFormSubmit, artificalEvent)
  list.forEach( processRowToSendResponse )

  function processRowToSendResponse(props){
    const columnReference = "Timestamp"
    const rowIdx = props.getRangeByColHeader(columnReference).getRow();
     process(rowIdx)
  }

function grabListOfResponsesToEmail(){
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheetName = "Form Responses 2"
  const sheet = ss.getSheetByName(sheetName)
  const { body } = getDataBySheet(sheet)
  return body.filter(blankEmailSent)

  function blankEmailSent(props){ return props["Email Sent"] === ""}
}

}
function onChangeListener(e){
  Logger.log(e)
  passSheetDataToOnFormSubmit()
}
