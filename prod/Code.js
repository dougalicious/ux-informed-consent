function passSheetDataToOnFormSubmit(){
  const list = grabListOfResponsesToEmail()
  const process = compose(__onFormSubmit, artificalEvent)
  list.forEach( processRowToSendResponse )

  function processRowToSendResponse(props){
    const columnReference = "Timestamp"
    const rowIdx = props.getRangeByColHeader(columnReference).getRow();
     process(rowIdx)
  }

function grabListOfResponsesToEmail(){
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheetName = "Form Responses 2"
  const sheet = ss.getSheetByName(sheetName)
  const { body } = getDataBySheet(sheet)
  return body.filter(blankEmailSent)

  function blankEmailSent(props){ return props["Email Sent"] === ""}
}

}
function dynamicChange(){
  Logger.log('dnc')
  passSheetDataToOnFormSubmit()
}
function dynamicEdit(){
  Logger.log('enc')
  // passSheetDataToOnFormSubmit()
}