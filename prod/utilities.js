
/* OWNER: douglascox, uxi-automation
 * SOURCES: 
 *
 * DESCRIPTION: Upon submission of go/studyrequestform, this script determines 
 * whether the requester is supported by Research Ops, determines which pod
 * should handle this request, and emails the appropriate pod
 *[OPTIONAL] @param {(sheet|object)} - reference sheet for data fetch
 *[OPTIONAL] @param {(number)} - row containing header 0-based index
 *[OPTIONAL] @param {(string|number)} - row where data start 0-based index
 *[OPTIONAL] @return {(object)} - properites of sheet for filter/update/etc
 */

/* returns properties of sheet
 * @param Object({Sheet})
 * @return { dataRange, body, headerObj, header, getValueByHeader }
 */
function getDataBySheet(sheet, headerIdx = 0, bodyRowStart = 1) {
  var dataRange = sheet.getDataRange();
  var values = dataRange.getValues()
  var headers = values.slice(headerIdx, headerIdx + 1)[0];
  var body = values.slice(bodyRowStart)
  var headerObj = headers.reduce(makeHeaderObj, {})
  var a = body.map(getValueByHeader)
  return {
    dataRange,
    body: body.map(getValueByHeader),
    headerObj,
    headers,
    getValueByHeader,
  }
  function makeHeaderObj(acc, next, idx) {
    if (next === "" || next === "-") {
      return acc;
    }
    if (acc.hasOwnProperty(next)) {
      debugger
      throw (`Duplicate headers found: "${next}"`)
    }
    acc[next] = idx
    return acc;
  }
  // transform Array(row) => Object()
  function getValueByHeader(row, rowIdx) {
    var rowAsObject = Object.create(headerObj)
    for (var header in headerObj) {
      var idx = headerObj[header]
      rowAsObject[header] = row[idx]
    }
    const getValueByHeader = (header) => {
      try {
        return sheet.getRange(rowIdx + 1 + bodyRowStart, headerObj[header] + 1)
      } catch (e) {
        throw (`unable to find header: ${header}`)
      }
    }
    Object.defineProperties(rowAsObject, {
      getRangeByColHeader: {
        value: getValueByHeader,
        writable: false,
        enumerable: false,
      },
    })

    return rowAsObject
  }
  /*
  value: tryCatch(`header: ${header} not found`, getValueByHeader),
      
  function tryCatch(errorMsg, fn){
    try{
      fn()
    } catch(e){
      throw (errorMsg +)
    }
  }
}
  */
}

// const compose = (...fns) => x => fns.reduceRight((y, f) => f(y), x);
function compose(...fns) {
  return x => fns.reduceRight((y, f) => f(y), x);
}

function artificalEvent( rowIdx ) {
  var ss = SpreadsheetApp.getActiveSpreadsheet()
  var sheet = ss.getActiveSheet()
  sheet = ss.getSheetByName("Form Responses 2")
  var { body, headerObj } = getDataBySheet(sheet)
  var defaultRow = 0 // is row 26 +2 0 base and header removed
  var activeRowIdx = rowIdx ? rowIdx : SpreadsheetApp.getActiveRange().getRow()
  var activeRowAsObj = activeRowIdx === -1 ? null : body[activeRowIdx - 2]
  
  Logger.log(`${defaultRow} default used`)
  for (let header in activeRowAsObj) {
    activeRowAsObj[header] = [activeRowAsObj[header]]
  }

  var range = {
    getValues: () => Object.keys(activeRowAsObj).map((header) => activeRowAsObj[header][0]),
    getRow: () => activeRowIdx === -1 ? null : activeRowIdx
  }
  var namedValues = { namedValues: activeRowAsObj }

  return Object.assign(namedValues, { range });
}


// new for informedconsent 2020
function countryMatchByRegex(countryLanguage1, countryLanguage2) {
  [countryLanguage1, countryLanguage2] = [countryLanguage1, countryLanguage2].map(str => str.replace(/\(|\)/g, "").replace(/_/g, " "))
  var regex = new RegExp(countryLanguage1, "gi")
  var res = regex.test(countryLanguage2)
  return res
}


function getRegionByCountry(){
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const regionMapSheet = ss.getSheetByName("Region Map")
  const { body } = getDataBySheet(regionMapSheet)
  return function byCountries( countries ){
    let countriesAsArr = countries.includes(",") ? countries.split(",").map(str => str.trim()) : countries
    let regions = body.filter(props => countriesAsArr.includes(props["Country"]) )
                      .reduce( (acc, next) => acc.includes(next["Region"]) ? acc : acc.concat(next["Region"]), [])
    debugger
    return regions
  }
}