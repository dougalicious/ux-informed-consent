function keyColorDataTypeGenerator(){
  const answers = getFormQuestions()
  const dataTypeRange = getDataTypeAnswerRange()
  dataTypeRange.forEach( (props) => {
    const answer = props["Form Text"]
    let range = props.getRangeByColHeader("Form Text")
    let v = range.getValue()
    let color= { green: "#d9ead3", red: "f4cccc"}
    debugger
    if( answers.includes(answer)){
      updateColor(range, color["green"])
    } else{
      updateColor(range, color["red"])
    }
      
  })
function updateColor(range, color){
  // const ss = SpreadsheetApp.getActiveSpreadsheet()
  // const sheet = ss.getSheetByName("Key");
  // const range = sheet.getRange(a1notation)
  range.setBackground(color)
}
function getDataTypeAnswerRange() {
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheet = ss.getSheetByName("Key");
  const values = sheet.getRange(2,1, sheet.getLastRow(), 1).getValues()
  const lastIdx = values.findIndex( row => row[0] === 'dataTypesNone') +1
  
  const { body }= getDataBySheet(sheet)

  return body.slice(0,lastIdx)
}
function getFormQuestions(){
    const formUrl = SpreadsheetApp.getActiveSpreadsheet().getFormUrl()
    const form = FormApp.openByUrl(formUrl)
    
    const items = form.getItems()
    const  [dataTypeQuestion]  = items.filter(item => item.getTitle() === "What type of data are you collecting in the study? (Check all that apply)") || dataTypeQnotFound()
    
    return dataTypeQuestion.asCheckboxItem()
                                    .getChoices()
                                    .map( prop => prop.getValue())

    function dataTypeQnotFound(){
      throw ("Data Type Question Not found")
      return null
    }
}
}